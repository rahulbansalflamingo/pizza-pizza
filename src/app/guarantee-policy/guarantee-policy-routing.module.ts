import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GuaranteePolicyComponent } from './guarantee-policy/guarantee-policy.component';

const routes: Routes = [
  { path: 'guarantee-policy', component:  GuaranteePolicyComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuaranteePolicyRoutingModule { }
