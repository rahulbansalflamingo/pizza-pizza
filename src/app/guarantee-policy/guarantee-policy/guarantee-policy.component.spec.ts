import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuaranteePolicyComponent } from './guarantee-policy.component';

describe('GuaranteePolicyComponent', () => {
  let component: GuaranteePolicyComponent;
  let fixture: ComponentFixture<GuaranteePolicyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GuaranteePolicyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuaranteePolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
