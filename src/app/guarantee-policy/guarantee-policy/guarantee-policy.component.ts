import { DOCUMENT } from '@angular/common';
import { Component, OnInit, Inject, Renderer2 } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-guarantee-policy',
  templateUrl: './guarantee-policy.component.html',
  styleUrls: ['./guarantee-policy.component.scss']
})
export class GuaranteePolicyComponent implements OnInit {

  contents:any;
  loaderHold:boolean = false;
  constructor(@Inject(DOCUMENT) private document: Document, private renderer: Renderer2, private dataservice: DataService, private apiService:ApiServiceService) { 
    this.dataservice.headerFooterHide(false);
  }

  ngOnInit(): void {
    this.loaderHold= true;
    this.renderer.addClass(this.document.body, 'inner-footer');
    this.apiService.staticPages("guarantee-policy").subscribe(async data=>{
      this.contents = await data.data[0].header;
      this.loaderHold= false;
    })
  }
  ngOnDestroy(): void {
    this.renderer.removeClass(this.document.body, 'inner-footer');
  }


}
