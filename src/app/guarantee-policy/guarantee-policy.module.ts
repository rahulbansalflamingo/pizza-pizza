import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GuaranteePolicyRoutingModule } from './guarantee-policy-routing.module';
import { GuaranteePolicyComponent } from './guarantee-policy/guarantee-policy.component';
import { SideHeaderModule } from '../side-header/side-header.module';
import { HelperModule } from '../helper/helper.module';
import { WebLoaderModule } from '../web-loader/web-loader.module';


@NgModule({
  declarations: [
    GuaranteePolicyComponent
  ],
  imports: [
    CommonModule,
    GuaranteePolicyRoutingModule,
    SideHeaderModule,
    HelperModule,
    WebLoaderModule
  ]
})
export class GuaranteePolicyModule { }
