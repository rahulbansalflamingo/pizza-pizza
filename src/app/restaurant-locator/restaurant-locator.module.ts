import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from '@agm/core';
import { FormsModule } from '@angular/forms';
import { RestaurantLocatorRoutingModule } from './restaurant-locator-routing.module';
import { RestaurantLocatorComponent } from './restaurant-locator/restaurant-locator.component';


@NgModule({
  declarations: [
    RestaurantLocatorComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RestaurantLocatorRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAQtOAXGzzIbMtR1xDXN8PIhlQgyUNhfm8',
      libraries: ['places']
    })
  ]
})
export class RestaurantLocatorModule { }
