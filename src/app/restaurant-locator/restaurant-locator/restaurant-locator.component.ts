import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { Subscription } from 'rxjs';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { LocationService } from 'src/app/services/location.service';
import { DataService } from 'src/app/services/data.service';
import { Router } from '@angular/router';
import { SeoService } from 'src/app/services/seo.service';

@Component({
  selector: 'app-restaurant-locator',
  templateUrl: './restaurant-locator.component.html',
  styleUrls: ['./restaurant-locator.component.scss']
})
export class RestaurantLocatorComponent implements OnInit {
  storedetailShow:boolean = false;
  bar:any ;
  storedatanotavail:any;
  private storeLocatorSubscription!: Subscription;
  private geoCoder: any;
  saveaddressShow:boolean= false;
  storeDetail:any;
 
  pizzaiconShow:boolean = true;
  searchiconShow:boolean = true;
  goLoader:boolean = false;
  nostoreAvailable:boolean = false;
  resetButton:boolean = false;
  @ViewChild('search')
   public searchElementRef!: ElementRef;
  value="''";
  constructor(
    private apiService:ApiServiceService, 
    private locationService:LocationService,
    private dataservice: DataService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private router: Router,
    private seoData:SeoService
  ) { 
    
  }

  ngOnInit(): void {
    this.seoData.updatemetatags({meta_title:"Restaurant Locator"});
    this.mapsAPILoader.load().then(() => {
      // this.getAddress();
      this.geoCoder = new google.maps.Geocoder;
      this.storedatanotavail="";
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
 
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
 
          //set latitude, longitude and zoom
          this.getAddress(place.geometry.location.lat(), place.geometry.location.lng());
        });
      });
    });
  }
  async getAddress(lati:any, long:any): Promise<void> { 

     
    this.geoCoder = new google.maps.Geocoder();
    this.geoCoder.geocode({ 'location': { lat: lati, lng: long } }, (results: any, status: any) => {
      if (status === 'OK') {
        if (results[0]) {
         
          var add = results[0].formatted_address;
          var value = add.split(",");
          
          this.searchiconShow = false;
          this.goLoader = true;
          let locateArray = {
           "lat":lati,
           "long":long,
           "city":value[value.length - 3].replace(/[0-9]/g, '')
        }
         this.storeLocatorSubscription = this.apiService.storeLocator(locateArray).subscribe(async store=>{
           
           if(store.status_code == 200){
            this.storedetailShow = true;
           
            this.storeDetail = store.data;
            
             
             this.pizzaiconShow = false;
            this.searchiconShow = true;
            this.goLoader = false;
             this.storedatanotavail="";
           }else{
            // this.storedetailShow = false;
            this.pizzaiconShow = true;
            this.nostoreAvailable = true;
            this.goLoader = false;
            this.searchiconShow = true;
           }
 
         })
          
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  
 }
  modelChangeFn(e:any){
    this.bar = e;
    if(e==""){
    
    }else{
     
    }
  }


  resetAddress(){
    this.storedetailShow = false;
    this.pizzaiconShow = true;
    this.nostoreAvailable = false;
    setTimeout(() => {
      this.resetButton = false;
    }, 100);
    
  }



  onKeyUp(x:any){
    if(x.target.value.length >=1){
      this.resetButton = true;
    }
    else{
      this.resetButton = false;
    }
  }


}
