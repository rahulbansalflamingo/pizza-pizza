import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RestaurantLocatorComponent } from './restaurant-locator/restaurant-locator.component';

const routes: Routes = [
  { path: 'restaurant-locator', component:  RestaurantLocatorComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestaurantLocatorRoutingModule { }
