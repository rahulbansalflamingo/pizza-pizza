import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForYouComponent } from './for-you/for-you.component';

const routes: Routes = [
  { path: 'for-you', component:  ForYouComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForYouRoutingModule { }
