import { DOCUMENT } from '@angular/common';
import { Component, OnInit, Inject, Renderer2 } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { DataService } from 'src/app/services/data.service';
import { SeoService } from 'src/app/services/seo.service';

@Component({
  selector: 'app-for-you',
  templateUrl: './for-you.component.html',
  styleUrls: ['./for-you.component.scss']
})
export class ForYouComponent implements OnInit {
  forYouForm: FormGroup;
  errormessage:any;
  orderText:boolean = true;
  loader:boolean = false;
  foryoudata:any;
  constructor(@Inject(DOCUMENT) private document: Document, private renderer: Renderer2,  private seoData:SeoService, private dataservice: DataService, formbuilder:FormBuilder,private apiService:ApiServiceService) {
    this.forYouForm = formbuilder.group({
      name:['', [Validators.required,Validators.minLength(3)]],
      phone: ['', [Validators.required,Validators.minLength(10),Validators.pattern("^[0-9]*$")]],
      email: ['', [Validators.required, Validators.email]],
      comments:['', [Validators.required]],
    })
   }

  ngOnInit(): void {
    this.renderer.addClass(this.document.body, 'inner-footer');
    this.dataservice.headerFooterHide(false);
    let script = this.renderer.createElement('script');
    script.type = "text/javascript";
    script.src = "assets/js/custom.js";
    this.apiService.staticPages("for-you").subscribe((data:any)=>{
      
      this.foryoudata =data.data[0].cms_data;
       this.seoData.updatemetatags(data.data[0]);
  
      this.renderer.appendChild(this.document.body, script);
    })
  }
  ngOnDestroy(): void {
    this.renderer.removeClass(this.document.body, 'inner-footer');
  }
  contactformdata = (forYouForm:any)=>{
    this.orderText = false;
    this.loader = true;
    var formdata = {
      "name": forYouForm.get('name')?.value,
      "phone": forYouForm.get('phone')?.value,
      "email": forYouForm.get('email')?.value,
      "comments": forYouForm.get('comments')?.value,
    }
    this.apiService.foryouform(formdata).subscribe(data=>{

// var errorid = <HTMLElement>document.getElementById('errorforyou');
if(data.status_code == 200){
  this.orderText = true;
    this.loader = false;
  // errorid.style.color = "green";
  this.renderer.addClass(this.document.body, 'error-green-show');
  this.renderer.removeClass(this.document.body, 'error-red-show');
  this.errormessage = data.message;
  this.forYouForm.reset();
}else{
  this.orderText = true;
    this.loader = false;
  // errorid.style.color = "red";
  this.renderer.removeClass(this.document.body, 'error-green-show');
  this.renderer.addClass(this.document.body, 'error-red-show');
  this.errormessage = data.message;
}
    })
    
  }
  /* alsoConfig-slider */
  howtoredeem = {
    arrows:true,
    nav:true,
    infinite: false,
    dots:false,
    slidesToShow: 2,
    slidesToScroll: 2,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  /* alsoConfig-end */


  id:any = ' ';
  accordion(ids:any){
    if (this.id == ids){
      this.id = ' ';
    }
  else{
    this.id= ids;
  }
}
scroll(el: HTMLElement) {
  el.scrollIntoView();
}


}
