import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { ForYouRoutingModule } from './for-you-routing.module';
import { ForYouComponent } from './for-you/for-you.component';
import { SideHeaderModule } from '../side-header/side-header.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HelperModule } from '../helper/helper.module';


@NgModule({
  declarations: [
    ForYouComponent
  ],
  imports: [
    CommonModule,
    ForYouRoutingModule,
    SlickCarouselModule,
    SideHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    HelperModule
  ]
})
export class ForYouModule { }
