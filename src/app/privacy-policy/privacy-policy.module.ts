import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrivacyPolicyRoutingModule } from './privacy-policy-routing.module';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { SideHeaderModule } from '../side-header/side-header.module';
import { HelperModule } from '../helper/helper.module';
import { WebLoaderModule } from '../web-loader/web-loader.module';


@NgModule({
  declarations: [
    PrivacyPolicyComponent,
  ],
  imports: [
    CommonModule,
    PrivacyPolicyRoutingModule,
    SideHeaderModule,
    HelperModule,
    WebLoaderModule
  ]
})
export class PrivacyPolicyModule { }
