import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebLoaderComponent } from './web-loader.component';

describe('WebLoaderComponent', () => {
  let component: WebLoaderComponent;
  let fixture: ComponentFixture<WebLoaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WebLoaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WebLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
