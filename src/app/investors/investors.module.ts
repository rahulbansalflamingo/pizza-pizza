import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvestorsRoutingModule } from './investors-routing.module';
import { InvestorsComponent } from './investors/investors.component';
import { SideHeaderModule } from '../side-header/side-header.module';
import { HelperModule } from '../helper/helper.module';


@NgModule({
  declarations: [
    InvestorsComponent
  ],
  imports: [
    CommonModule,
    InvestorsRoutingModule,
    SideHeaderModule,
    HelperModule
  ]
})
export class InvestorsModule { }
