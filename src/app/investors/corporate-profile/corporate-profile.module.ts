import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CorporateProfileRoutingModule } from './corporate-profile-routing.module';
import { CorporateProfileComponent } from './corporate-profile.component';
import { SideHeaderModule } from 'src/app/side-header/side-header.module';


@NgModule({
  declarations: [
    CorporateProfileComponent
  ],
  imports: [
    CommonModule,
    CorporateProfileRoutingModule,
    SideHeaderModule
  ]
})
export class CorporateProfileModule { }
