import { DOCUMENT } from '@angular/common';
import { Component, OnInit, Inject, Renderer2  } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-corporate-profile',
  templateUrl: './corporate-profile.component.html',
  styleUrls: ['./corporate-profile.component.scss']
})
export class CorporateProfileComponent implements OnInit {
  corporateLists= ['Corporate Structure', 'Corporate Directors'];
  corporateselectedList:any;

  constructor(@Inject(DOCUMENT) private document: Document, private renderer: Renderer2, private dataservice: DataService) { }

  ngOnInit(): void {
    this.renderer.addClass(this.document.body, 'inner-footer');
    this.dataservice.headerFooterHide(false);
    this.corporateselectedList = this.corporateLists[0];
  }
  ngOnDestroy(): void {
    this.renderer.removeClass(this.document.body, 'inner-footer');
  }


openMenuList(corporateList:any){
  this.corporateselectedList = corporateList;
}

}
