import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LeadershipTeamRoutingModule } from './leadership-team-routing.module';
import { LeadershipTeamComponent } from './leadership-team.component';
import { SideHeaderModule } from 'src/app/side-header/side-header.module';


@NgModule({
  declarations: [
    LeadershipTeamComponent
  ],
  imports: [
    CommonModule,
    LeadershipTeamRoutingModule,
    SideHeaderModule
  ]
})
export class LeadershipTeamModule { }
