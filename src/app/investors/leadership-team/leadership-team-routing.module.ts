import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LeadershipTeamComponent } from './leadership-team.component';
const routes: Routes = [
  { path: 'leadership-team', component:  LeadershipTeamComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LeadershipTeamRoutingModule { }
