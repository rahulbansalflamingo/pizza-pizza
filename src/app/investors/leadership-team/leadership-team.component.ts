import { DOCUMENT } from '@angular/common';
import { Component, OnInit, Inject, Renderer2  } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-leadership-team',
  templateUrl: './leadership-team.component.html',
  styleUrls: ['./leadership-team.component.scss']
})
export class LeadershipTeamComponent implements OnInit {
  constructor(@Inject(DOCUMENT) private document: Document, private renderer: Renderer2, private dataservice: DataService) { }

  ngOnInit(): void {
    this.renderer.addClass(this.document.body, 'inner-footer');
    this.dataservice.headerFooterHide(false);
  }
  ngOnDestroy(): void {
    this.renderer.removeClass(this.document.body, 'inner-footer');
  }


}
