import { DOCUMENT } from '@angular/common';

import { Component, OnInit, Inject, Renderer2  } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { DataService } from 'src/app/services/data.service';
import { SeoService } from 'src/app/services/seo.service';

@Component({
  selector: 'app-investors',
  templateUrl: './investors.component.html',
  styleUrls: ['./investors.component.scss']
})
export class InvestorsComponent implements OnInit {

  investorsdata:any;
  constructor(@Inject(DOCUMENT) private document: Document, private renderer: Renderer2, private seoData:SeoService, private dataservice: DataService, private apiService:ApiServiceService) { }

  ngOnInit(): void {
    this.renderer.addClass(this.document.body, 'inner-footer');
    this.dataservice.headerFooterHide(false);
    let script = this.renderer.createElement('script');
    script.type = "text/javascript";
    script.src = "assets/js/custom.js";
    this.apiService.staticPages("investors").subscribe((data:any)=>{
      
      this.investorsdata =data.data[0].cms_data;
       this.seoData.updatemetatags(data.data[0]);
    
      this.renderer.appendChild(this.document.body, script);
    })
  }
  ngOnDestroy(): void {
    this.renderer.removeClass(this.document.body, 'inner-footer');
  }


  scroll(el: HTMLElement) {
    el.scrollIntoView();
  }

  id:any = ' ';
  accordion(ids:any){
    if (this.id == ids){
      this.id = ' ';
    }
  else{
    this.id= ids;
  }
}

}
