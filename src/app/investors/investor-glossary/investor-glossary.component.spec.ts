import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestorGlossaryComponent } from './investor-glossary.component';

describe('InvestorGlossaryComponent', () => {
  let component: InvestorGlossaryComponent;
  let fixture: ComponentFixture<InvestorGlossaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvestorGlossaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestorGlossaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
