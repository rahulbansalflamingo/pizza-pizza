import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InvestorGlossaryComponent } from './investor-glossary.component';

const routes: Routes = [
  {path: 'investor-glossary', component:  InvestorGlossaryComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvestorGlossaryRoutingModule { }
