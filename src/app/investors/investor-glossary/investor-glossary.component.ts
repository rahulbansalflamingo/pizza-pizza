import { DOCUMENT } from '@angular/common';
import { Component, OnInit, Inject, Renderer2 } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-investor-glossary',
  templateUrl: './investor-glossary.component.html',
  styleUrls: ['./investor-glossary.component.scss']
})
export class InvestorGlossaryComponent implements OnInit {

  constructor(@Inject(DOCUMENT) private document: Document, private renderer: Renderer2, private dataservice: DataService) { }

  ngOnInit(): void {
    this.renderer.addClass(this.document.body, 'inner-footer');
    this.dataservice.headerFooterHide(false);
  }
  ngOnDestroy(): void {
    this.renderer.removeClass(this.document.body, 'inner-footer');
  }



}
