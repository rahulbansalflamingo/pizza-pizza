import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvestorGlossaryRoutingModule } from './investor-glossary-routing.module';
import { InvestorGlossaryComponent } from './investor-glossary.component';
import { SideHeaderModule } from 'src/app/side-header/side-header.module';

@NgModule({
  declarations: [
    InvestorGlossaryComponent
  ],
  imports: [
    CommonModule,
    InvestorGlossaryRoutingModule,
    SideHeaderModule
  ]
})
export class InvestorGlossaryModule { }
