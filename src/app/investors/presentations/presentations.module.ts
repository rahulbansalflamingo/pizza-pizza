import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PresentationsRoutingModule } from './presentations-routing.module';
import { PresentationsComponent } from './presentations.component';
import { SideHeaderModule } from 'src/app/side-header/side-header.module';


@NgModule({
  declarations: [
    PresentationsComponent
  ],
  imports: [
    CommonModule,
    PresentationsRoutingModule,
    SideHeaderModule
  ]
})
export class PresentationsModule { }
