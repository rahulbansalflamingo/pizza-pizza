import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinancialReportsRoutingModule } from './financial-reports-routing.module';
import { FinancialReportsComponent } from './financial-reports.component';
import { SideHeaderModule } from 'src/app/side-header/side-header.module';


@NgModule({
  declarations: [
    FinancialReportsComponent
  ],
  imports: [
    CommonModule,
    FinancialReportsRoutingModule,
    SideHeaderModule
  ]
})
export class FinancialReportsModule { }
