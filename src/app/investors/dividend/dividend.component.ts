import { DOCUMENT } from '@angular/common';
import { Component, OnInit, Inject, Renderer2  } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-dividend',
  templateUrl: './dividend.component.html',
  styleUrls: ['./dividend.component.scss']
})
export class DividendComponent implements OnInit {
  dividendLists= ['2022', '2021', '2020', '2019', '2018', '2017', '2016'];
  dividendselectedList:any;

  constructor(@Inject(DOCUMENT) private document: Document, private renderer: Renderer2, private dataservice: DataService) { }

  ngOnInit(): void {
    this.renderer.addClass(this.document.body, 'inner-footer');
    this.dataservice.headerFooterHide(false);
    this.dividendselectedList = this.dividendLists[0];
  }
  ngOnDestroy(): void {
    this.renderer.removeClass(this.document.body, 'inner-footer');
  }



openMenuList(dividendList:any){
  this.dividendselectedList = dividendList;
}

}
