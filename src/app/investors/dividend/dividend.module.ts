import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DividendRoutingModule } from './dividend-routing.module';
import { DividendComponent } from './dividend.component';
import { SideHeaderModule } from 'src/app/side-header/side-header.module';


@NgModule({
  declarations: [
    DividendComponent
  ],
  imports: [
    CommonModule,
    DividendRoutingModule,
    SideHeaderModule
  ]
})
export class DividendModule { }
