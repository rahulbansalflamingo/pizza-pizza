import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DividendComponent } from './dividend.component';

const routes: Routes = [
  { path: 'dividend', component:  DividendComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DividendRoutingModule { }
