import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvestorFaqsRoutingModule } from './investor-faqs-routing.module';
import { InvestorFaqsComponent } from './investor-faqs.component';
import { SideHeaderModule } from 'src/app/side-header/side-header.module';


@NgModule({
  declarations: [
    InvestorFaqsComponent
  ],
  imports: [
    CommonModule,
    InvestorFaqsRoutingModule,
    SideHeaderModule
  ]
})
export class InvestorFaqsModule { }
