import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InvestorFaqsComponent } from './investor-faqs.component';

const routes: Routes = [
  { path: 'investor-faqs', component:  InvestorFaqsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvestorFaqsRoutingModule { }
