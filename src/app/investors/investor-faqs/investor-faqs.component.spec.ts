import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestorFaqsComponent } from './investor-faqs.component';

describe('InvestorFaqsComponent', () => {
  let component: InvestorFaqsComponent;
  let fixture: ComponentFixture<InvestorFaqsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvestorFaqsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestorFaqsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
