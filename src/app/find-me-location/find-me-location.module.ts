import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from '@agm/core';
import { FindMeLocationComponent } from './find-me-location.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    FindMeLocationComponent
  ],
  exports:[FindMeLocationComponent],
  imports: [
    CommonModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAQtOAXGzzIbMtR1xDXN8PIhlQgyUNhfm8',
      libraries: ['places']
    })
  ]
})
export class FindMeLocationModule { }
