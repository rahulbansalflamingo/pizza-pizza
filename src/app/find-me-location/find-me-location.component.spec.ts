import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FindMeLocationComponent } from './find-me-location.component';

describe('FindMeLocationComponent', () => {
  let component: FindMeLocationComponent;
  let fixture: ComponentFixture<FindMeLocationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FindMeLocationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FindMeLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
