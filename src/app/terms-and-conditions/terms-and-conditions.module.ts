import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TermsAndConditionsRoutingModule } from './terms-and-conditions-routing.module';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { SideHeaderModule } from '../side-header/side-header.module';
import { HelperModule } from '../helper/helper.module';
import { WebLoaderModule } from '../web-loader/web-loader.module';


@NgModule({
  declarations: [
    TermsAndConditionsComponent
  ],
  imports: [
    CommonModule,
    TermsAndConditionsRoutingModule,
    SideHeaderModule,
    HelperModule,
    WebLoaderModule
  ]
})
export class TermsAndConditionsModule { }
