import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FranchisingComponent } from './franchising/franchising.component';
const routes: Routes = [
  { path: 'franchising', component:  FranchisingComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FranchisingRoutingModule { }
