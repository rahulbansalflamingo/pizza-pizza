import { DOCUMENT } from '@angular/common';
import { Component, OnInit, Inject, Renderer2} from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { SeoService } from 'src/app/services/seo.service';
@Component({
  selector: 'app-franchising',
  templateUrl: './franchising.component.html',
  styleUrls: ['./franchising.component.scss']
})
export class FranchisingComponent implements OnInit {
  franchisiForm:FormGroup
  siteKey:any
  errormessage:any;
  orderText:boolean = true;
  loader:boolean = false;
  franchisedata:any;
  constructor(@Inject(DOCUMENT) private document: Document, private renderer: Renderer2,private seoData:SeoService,  private dataservice: DataService,formbuilder:FormBuilder,private apiService:ApiServiceService) {
    this.franchisiForm = formbuilder.group({
      fname: ['', [Validators.required]],
      lname: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]], 
      phone: ['', [Validators.required, Validators.minLength(10), Validators.pattern("^[0-9]*$")]],
      bestime: ['', [Validators.required]],
      province: ['', [Validators.required]],
      cityInterest:['', [Validators.required]],
      street:[''],
      city:[''],
      addressprovince:[''],
      postalcode:[''],
      investmentcapital:['', [Validators.required]],
      recaptcha: ['', Validators.required]
    })
   }

  ngOnInit(): void {
    this.siteKey = "6Lf9CgshAAAAABvoOj8-hjkoQNU3Jm3l3nORaoSl"
    this.dataservice.headerFooterHide(false);
    this.renderer.addClass(this.document.body, 'inner-footer');

    let script = this.renderer.createElement('script');
    script.type = "text/javascript";
    script.src = "assets/js/custom.js";

    

    this.apiService.staticPages("franchise").subscribe((data:any)=>{
      this.seoData.updatemetatags(data.data[0]);
      console.log(data.data[0]);
      this.franchisedata =data.data[0].cms_data;
      
      this.renderer.appendChild(this.document.body, script);
    })
    
  }

  ngOnDestroy(): void {
    this.renderer.removeClass(this.document.body, 'inner-footer');
  }

  contactformdata = (franchisiForm:any)=>{

    var formdata = {
          "first_name": franchisiForm.get('fname')?.value,
          "last_name": franchisiForm.get('lname')?.value,
          "email": franchisiForm.get('email')?.value,
          "phone": franchisiForm.get('phone')?.value,
          "province_of_interest": franchisiForm.get('province')?.value,
          "city_of_interest": franchisiForm.get('cityInterest')?.value,
          "available_investment_capital": franchisiForm.get('investmentcapital')?.value,
          "best_time_to_call": franchisiForm.get('bestime')?.value,
          "street": franchisiForm.get('street')?.value,
          "city": franchisiForm.get('city')?.value,
          "province": franchisiForm.get('addressprovince')?.value,
          "postal_code": franchisiForm.get('postalcode')?.value
      }
      this.apiService.franchisingForm(formdata).subscribe(data=>{
      
        // var errorid = <HTMLElement>document.getElementById('errorforyou');
        if(data.status_code == 200){
          this.orderText = true;
            this.loader = false;
            this.renderer.addClass(this.document.body, 'error-green-show');
            this.renderer.removeClass(this.document.body, 'error-red-show');
          // errorid.style.color = "green";
          this.errormessage = data.message;
          this.franchisiForm.reset();
        }else{
          this.orderText = true;
            this.loader = false;
            this.renderer.removeClass(this.document.body, 'error-green-show');
            this.renderer.addClass(this.document.body, 'error-red-show');
          // errorid.style.color = "red";
          this.errormessage = data.message;
        }
      })

  }
  scroll(el: HTMLElement) {
    el.scrollIntoView();
  }


id:any = ' ';
accordion(ids:any){
  if (this.id == ids){
    this.id = ' ';
  }
else{
  this.id= ids;
}
}

}
