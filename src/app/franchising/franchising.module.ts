import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FranchisingRoutingModule } from './franchising-routing.module';
import { FranchisingComponent } from './franchising/franchising.component';
import { SideHeaderModule } from '../side-header/side-header.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';
import { HelperModule } from '../helper/helper.module';


@NgModule({
  declarations: [
    FranchisingComponent
  ],
  imports: [
    CommonModule,
    FranchisingRoutingModule,
    SideHeaderModule,
    FormsModule,
    ReactiveFormsModule,
    NgxCaptchaModule,
    HelperModule
  ]
})
export class FranchisingModule { }
