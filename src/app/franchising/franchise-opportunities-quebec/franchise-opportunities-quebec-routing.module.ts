import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FranchiseOpportunitiesQuebecComponent} from './franchise-opportunities-quebec.component';
const routes: Routes = [
  { path: 'franchise-opportunities-quebec', component:  FranchiseOpportunitiesQuebecComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FranchiseOpportunitiesQuebecRoutingModule { }
