import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FranchiseOpportunitiesQuebecRoutingModule } from './franchise-opportunities-quebec-routing.module';
import { FranchiseOpportunitiesQuebecComponent } from './franchise-opportunities-quebec.component';
import { SideHeaderModule } from 'src/app/side-header/side-header.module';

@NgModule({
  declarations: [
    FranchiseOpportunitiesQuebecComponent
  ],
  imports: [
    CommonModule,
    FranchiseOpportunitiesQuebecRoutingModule,
    SideHeaderModule
  ]
})
export class FranchiseOpportunitiesQuebecModule { }
