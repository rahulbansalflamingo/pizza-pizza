import { DOCUMENT } from '@angular/common';
import { Component, OnInit, Inject, Renderer2  } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-franchise-opportunities-quebec',
  templateUrl: './franchise-opportunities-quebec.component.html',
  styleUrls: ['./franchise-opportunities-quebec.component.scss']
})
export class FranchiseOpportunitiesQuebecComponent implements OnInit {

  constructor(@Inject(DOCUMENT) private document: Document, private renderer: Renderer2, private dataservice: DataService) { }

  ngOnInit(): void {
    this.renderer.addClass(this.document.body, 'inner-footer');
    this.dataservice.headerFooterHide(false);
  }
  ngOnDestroy(): void {
    this.renderer.removeClass(this.document.body, 'inner-footer');
  }

}
