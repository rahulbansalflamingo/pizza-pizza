import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FranchiseOpportunitiesQuebecComponent } from './franchise-opportunities-quebec.component';

describe('FranchiseOpportunitiesQuebecComponent', () => {
  let component: FranchiseOpportunitiesQuebecComponent;
  let fixture: ComponentFixture<FranchiseOpportunitiesQuebecComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FranchiseOpportunitiesQuebecComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FranchiseOpportunitiesQuebecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
