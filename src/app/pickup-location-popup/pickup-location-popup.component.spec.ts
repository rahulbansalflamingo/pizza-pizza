import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PickupLocationPopupComponent } from './pickup-location-popup.component';

describe('PickupLocationPopupComponent', () => {
  let component: PickupLocationPopupComponent;
  let fixture: ComponentFixture<PickupLocationPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PickupLocationPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PickupLocationPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
