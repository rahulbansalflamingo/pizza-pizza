import { DOCUMENT } from '@angular/common';
import { Component, OnInit, Inject, Renderer2 } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-accessibility',
  templateUrl: './accessibility.component.html',
  styleUrls: ['./accessibility.component.scss']
})
export class AccessibilityComponent implements OnInit {
  contents:any;
  header:any;
  contentone:any;
  contenttwo:any;
  aacordianOne:any;
  accordisantwo:any;
  loaderHold:boolean= false;
  constructor(@Inject(DOCUMENT) private document: Document, private renderer: Renderer2, private dataservice: DataService, private apiService:ApiServiceService) { 
    this.dataservice.headerFooterHide(false);
  }

  ngOnInit(): void {
this.loaderHold = true;
    this.renderer.addClass(this.document.body, 'inner-footer');
    let script = this.renderer.createElement('script');
    script.type = "text/javascript";
    script.src = "assets/js/custom.js";
    
    this.apiService.staticPages("accessibility").subscribe(async data=>{
      this.contents = await data.data[0].cms_data.SECTION1[0].only_html;
    
      this.renderer.appendChild(this.document.body, script);
      this.loaderHold = false;;
    })
    
  }
  ngOnDestroy(): void {
    this.renderer.removeClass(this.document.body, 'inner-footer');
  }


id:any = ' ';
  accordion(ids:any){
    if (this.id == ids){
      this.id = ' ';
    }
  else{
    this.id= ids;
  }
}

}
