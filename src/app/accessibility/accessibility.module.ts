import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccessibilityRoutingModule } from './accessibility-routing.module';
import { AccessibilityComponent } from './accessibility/accessibility.component';
import { SideHeaderModule } from '../side-header/side-header.module';
import { HelperModule } from '../helper/helper.module';
import { WebLoaderModule } from '../web-loader/web-loader.module';


@NgModule({
  declarations: [
    AccessibilityComponent
  ],
  imports: [
    CommonModule,
    AccessibilityRoutingModule,
    SideHeaderModule,
    HelperModule,
    WebLoaderModule
  ]
})
export class AccessibilityModule { }
