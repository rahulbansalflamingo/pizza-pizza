import { DOCUMENT } from '@angular/common';
import { Component, OnInit, Inject, Renderer2 } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { DataService } from 'src/app/services/data.service';
import { SeoService } from 'src/app/services/seo.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {
companydata:any;
  constructor(@Inject(DOCUMENT) private document: Document, private renderer: Renderer2,private seoData:SeoService, private dataservice: DataService, private apiService: ApiServiceService) { }

  async ngOnInit() {
    this.renderer.addClass(this.document.body, 'inner-footer');
    this.dataservice.headerFooterHide(false);
    let script = this.renderer.createElement('script');
    script.type = "text/javascript";
    script.src = "assets/js/custom.js";
     this.apiService.company().subscribe( async data=>{
      this.companydata =  data.data[0].cms_data;
      await this.seoData.updatemetatags(data.data[0]);
      
      this.renderer.appendChild(this.document.body, script);
    })
  }
  ngOnDestroy(): void {
    this.renderer.removeClass(this.document.body, 'inner-footer');
  }

  id:any = ' ';
  accordion(ids:any){
    if (this.id == ids){
      this.id = ' ';
    }
  else{
    this.id= ids;
  }
  }


  }


