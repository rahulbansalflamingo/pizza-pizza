import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountsPayableDataEntryClerkComponent } from './accounts-payable-data-entry-clerk.component';

describe('AccountsPayableDataEntryClerkComponent', () => {
  let component: AccountsPayableDataEntryClerkComponent;
  let fixture: ComponentFixture<AccountsPayableDataEntryClerkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountsPayableDataEntryClerkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountsPayableDataEntryClerkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
