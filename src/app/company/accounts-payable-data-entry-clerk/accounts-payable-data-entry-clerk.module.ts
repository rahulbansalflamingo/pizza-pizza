import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountsPayableDataEntryClerkRoutingModule } from './accounts-payable-data-entry-clerk-routing.module';
import { AccountsPayableDataEntryClerkComponent } from './accounts-payable-data-entry-clerk.component';
import { SideHeaderModule } from 'src/app/side-header/side-header.module';

@NgModule({
  declarations: [
    AccountsPayableDataEntryClerkComponent
  ],
  imports: [
    CommonModule,
    AccountsPayableDataEntryClerkRoutingModule,
    SideHeaderModule
  ]
})
export class AccountsPayableDataEntryClerkModule { }
