import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountsPayableDataEntryClerkComponent } from './accounts-payable-data-entry-clerk.component';

const routes: Routes = [
  { path: 'accounts-payable-data-entry-clerk', component:  AccountsPayableDataEntryClerkComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountsPayableDataEntryClerkRoutingModule { }
