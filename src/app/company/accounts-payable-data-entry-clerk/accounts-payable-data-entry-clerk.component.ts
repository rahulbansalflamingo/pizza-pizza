import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-accounts-payable-data-entry-clerk',
  templateUrl: './accounts-payable-data-entry-clerk.component.html',
  styleUrls: ['./accounts-payable-data-entry-clerk.component.scss']
})
export class AccountsPayableDataEntryClerkComponent implements OnInit {


  constructor(private dataservice: DataService) { }

  ngOnInit(): void {
    this.dataservice.headerFooterHide(false);
  }


}
