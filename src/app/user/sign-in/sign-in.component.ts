import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild, Inject, Renderer2  } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, NgForm, Validator, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { ApiServiceService } from 'src/app/services/api-service.service';
// import { SocialAuthService } from '@abacritt/angularx-social-login';
// import { FacebookLoginProvider, GoogleLoginProvider } from '@abacritt/angularx-social-login';
// import { SocialUser } from "@abacritt/angularx-social-login";
import { json } from 'express';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})

export class SignInComponent implements OnInit {
  array:any =[];
  show:any = false;
  loginForm: FormGroup;
  loginError: any;
  socialloginError:any;
  signText: boolean = true;
  loader:boolean = false;
  fbLoader:boolean = false;
  ggLoader:boolean = false;
  forgotpassword:FormGroup;
  auth2:any;
  forgotloader:boolean = false;
  forgotSuccessMessage:any;
  forgotmessageflag:any
  forgetcontinueLink:boolean = false;
  forgetpasswordlink:boolean = true;
  // public user: SocialUser = new SocialUser
  // public user: any;
  public loggedIn!: boolean;
  constructor(@Inject(DOCUMENT) private document: Document, private renderer: Renderer2, private formbuilder:FormBuilder, private apiService:ApiServiceService, private router: Router,
   //private authService: SocialAuthService, 
   private dataservice: DataService) { 
    this.loginForm = formbuilder.group({
      emailId: ['', [Validators.required, Validators.email, Validators.pattern("[A-Za-z0-9.%-]+@[A-Za-z0-9.%-]+\\.[A-a-Z-z]{2,3}")]],
      userPassword: ['', [Validators.required, Validators.minLength(7)]]
    })
    this.forgotpassword = formbuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.pattern("[A-Za-z0-9.%-]+@[A-Za-z0-9.%-]+\\.[A-a-Z-z]{2,3}")]]
    })
  }

  // @ViewChild('loginRef', {static: true }) loginElement!: ElementRef;

  ngOnInit(): void {
    if(localStorage.getItem("userData")){
      this.router.navigate(['/']);
     }
    this.renderer.addClass(this.document.body, 'footer-cart-h');
    this.dataservice.cartshow(true);
    // this.authService.authState.subscribe((user) => {
    //   console.log(user);
    //   this.user = user;
 
    //   this.loggedIn = user != null;
    // });
    // this.googleAuthSDK();
  }
  ngOnDestroy(): void {
    this.renderer.removeClass(this.document.body, 'footer-cart-h');
  }

  // callLoginButton() {
     
  //   this.auth2.attachClickHandler(this.loginElement.nativeElement, {},
  //     (googleAuthUser:any) => {
     
  //       let profile = googleAuthUser.getBasicProfile();
  //       console.log('Token || ' + googleAuthUser.getAuthResponse().id_token);
  //       console.log('ID: ' + profile.getId());
  //       console.log('Name: ' + profile.getName());
  //       console.log('Image URL: ' + profile.getImageUrl());
  //       console.log('Email: ' + profile.getEmail());
    
  //     }, (error:any) => {
  //       console.log(JSON.stringify(error, undefined, 2));
  //     });
 
  // }

  // googleAuthSDK() {
     
  //   (<any>window)['googleSDKLoaded'] = () => {
  //     (<any>window)['gapi'].load('auth2', () => {
  //       this.auth2 = (<any>window)['gapi'].auth2.getAuthInstance({
  //         client_id: '870073479312-vjdblgcu2vkj1krslj3cqsq7rlemh1u5.apps.googleusercontent.com',
  //         cookiepolicy: 'single_host_origin',
  //         scope: 'profile email'
  //       });
  //       this.callLoginButton();
  //     });
  //   }
     
  //   (function(d, s, id){
  //     var js, fjs = d.getElementsByTagName(s)[0];
  //     if (d.getElementById(id)) {return;}
  //     js = d.createElement('script'); 
  //     js.id = id;
  //     js.src = "https://apis.google.com/js/platform.js?onload=googleSDKLoaded";
  //     fjs?.parentNode?.insertBefore(js, fjs);
  //   }(document, 'script', 'google-jssdk'));
   
  // }







   //Login
  //  signInWithFB(): void {
  //    this.fbLoader = true;
  //   this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(
  //     d => {
      
  //       // var abc: any = [];
  //       this.apiService.socialLogin(d).subscribe(data=>{
        
  //         if(data.status_code == 200){
  //           localStorage.setItem('user', JSON.stringify(data));
  //           localStorage.setItem('userData', JSON.stringify(data.user));
  //           this.addressList(data.user)
  //           this.dataservice.changeMessage("logedIn");
  //           this.dataservice.logincheckfunction(true);
  //           this.fbLoader = false;
  //           this.router.navigate(['/']);
  //           // setTimeout(() => {
  //           //   window.location.reload();
  //           //  }, 1000);
  //         }else{
  //           this.fbLoader = false;
            
  //         }
  //       },
  //       error=>{
  //         console.log(error);
  //         this.fbLoader = false;
  //       })

  //     })
  //   .catch(error => {
  //     this.fbLoader = false;
  //     console.log(error);
  //     this.socialloginError = error.error;
  //   });;
  // }
  // withGoogle(): void {
  //   this.ggLoader = true;
  //   this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(
  //     d => {
      
  //       this.apiService.socialLogin(d).subscribe(data=>{
       
  //         if(data.status_code == 200){
  //           localStorage.setItem('user', JSON.stringify(data));
  //           localStorage.setItem('userData', JSON.stringify(data.user));
  //           this.addressList(data.user)
  //           this.dataservice.changeMessage("logedIn");
  //           this.dataservice.logincheckfunction(true);
  //           this.fbLoader = false;
  //           this.router.navigate(['/']);
  //           // setTimeout(() => {
  //           //   window.location.reload();
  //           //  }, 1000);
  //         }else{
  //           this.fbLoader = false;
            
  //         }
  //       },
  //       error=>{
  //         console.log(error);
  //         this.fbLoader = false;
  //       })
  //     }
  //     )
  //   .catch(error => {
  //     console.log(error);
  //     // this.socialloginError = error.error;
  //     this.ggLoader = false;
  //   });
  // }
  // Logout Function
  // signOut(): void {
  //   this.authService.signOut();
  // }

  showPassword(e:any){
   
    if(e.target.checked){
      this.show= true;
    }else{
      this.show= false;
    }
    
  }
  // forgot password function
  forgotpasswordfunction(inputdata:any){
    this.forgotloader=true;

var forgotemail = {
  "auth_user_email":inputdata.value.email
}
this.apiService.forgotpassword(forgotemail).subscribe(data=>{
  console.log(data)

  if(data.status_code == 200){
    this.forgotSuccessMessage = data.message;
    this.forgotmessageflag = 1
    this.forgotloader=false;
    this.forgotpassword.reset();
    this.forgetcontinueLink = true;
  this.forgetpasswordlink = false;
    setTimeout(() => {
      this.forgotSuccessMessage = '';
    }, 5000);
  }else{
    this.forgotSuccessMessage = data.message?data.message : data.error;
    this.forgotmessageflag = 0
    this.forgotloader=false;
  }
})

  }

  fortgotcontinueclose(){
    document.body.classList.remove('password-open')
    this.forgetcontinueLink = false;
    this.forgetpasswordlink = true;
  }
   postData(loginForm:any){
    this.signText = false;
    this.loader = true;
    let logindata;
    if(localStorage.getItem("session_id")){
      logindata ={"email":this.loginForm.get('emailId')?.value, "password":this.loginForm.get('userPassword')?.value,  "session_id": JSON.parse(localStorage.getItem("session_id") || "null")}
    }else{
      logindata ={"email":this.loginForm.get('emailId')?.value, "password":this.loginForm.get('userPassword')?.value,  "session_id": "null"}

    }
    
 
     this.apiService.login(logindata).subscribe(data=>{
       this.array = data;

      if(this.array.status_code == 200){
        localStorage.setItem('user', JSON.stringify(data));
        localStorage.setItem('userData', JSON.stringify(data.user));
        this.dataservice.changeMessage("logedIn");
        this.dataservice.logincheckfunction(true);
        this.addressList(data.user)
        this.signText = true;
        this.loader = false;
        this.dataservice.productItem();
        this.router.navigate(['/']);
      //  setTimeout(() => {
      //   window.location.reload();
      //  }, 1000);
      }else{
        this.signText = true;
        this.loader = false;
        this.loginError = data.error
      }
     },
     error =>{
       console.log(error);
     })
  }
  addressList(userData:any){
    if(localStorage.getItem("pickupDeliverystores")){
      this.apiService.listUSerPickUp(userData.id).subscribe(list=>{
        var addressData = [];
        console.log(list)
        if(list.status_code == 200 && list.data.length > 0){
          addressData = list.data.filter((data:any)=>{
              return data.id != JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").data.id
          })
          addressData.push(JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").data) 
          const updatedStoreList = addressData.map((obj:any) =>
    obj.id === JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").data.id ? { ...obj, by_default: 1 } : obj
  );
  localStorage.setItem('savedPickupLocation', JSON.stringify(updatedStoreList));
  
        }else{
          
          addressData.push(
            JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").data
          )
          addressData.map((obj:any)=>{
            obj.by_default = 1;
          })
          localStorage.setItem('savedPickupLocation', JSON.stringify(addressData));
        }
      })
    }
    
  }
  PasswordPopUpShow(): void {
    document.body.classList.add('password-open')
  }
  PasswordPopUpHide(): void {
    document.body.classList.remove('password-open')
  }
}
