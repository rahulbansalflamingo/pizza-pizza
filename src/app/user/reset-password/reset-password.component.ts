import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, NgForm, Validator, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmPasswordValidator } from 'src/app/helper/confirmed.validators'
import { ApiServiceService } from 'src/app/services/api-service.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  Paymentmore:boolean =true;
  Paymentuser:boolean = true;
  Addressesmore:boolean =true;
  Addressesuser:boolean = true;
  PaymentShown: boolean = false ;
  AddressesShown: boolean = false;
  localStorageData:any
  userdata:any
  url:any
  show:any = 0;
  days:any = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
  years:any = []
  successMessage:any
  changepassMessage:any;
  changepassRes: boolean = false;
  pickupPopupLocation:boolean = false;
  showPickupStore:any;
  deletePickupItemID:any;
  deletePickupItemValue:any;
  seeMenu:boolean= false;
  orderHistory:boolean = false;
  paymentaddesSuccessMessage:any;
  paymentmessageflag:any;
  paysaveloader:boolean = false;
  changePassword:FormGroup;
  resetpassLoader:boolean = false;
  constructor(private formbuilder:FormBuilder,private router: Router,private activatedRoute: ActivatedRoute, private dataservice: DataService, private apiService:ApiServiceService,) { 
    this.changePassword = formbuilder.group({
      newPassword: ['',[Validators.required, Validators.minLength(7), Validators.pattern("^(?=.*?[A-Za-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{7,}$")]],
      confirmPassword: ['',[Validators.required, Validators.minLength(7)]],
    },
          {
            validator: ConfirmPasswordValidator("newPassword", "confirmPassword")
          })
  }

  ngOnInit(): void {
    this.dataservice.cartshow(false);
    this.signOut();
    jQuery(".find-popup").removeClass("is-visible");
    
  }
  get f(){
    return  this.changePassword.controls;
  }
  updatePassword(changePaword:any){
    this.resetpassLoader = true;
    var updatepassArray = {
      "user_id":null,
      "token":this.activatedRoute.snapshot.params["resetToken"],
      "password": this.changePassword.get("newPassword")?.value
    }
 
    this.apiService.updatePassword(updatepassArray).subscribe(data=>{
  
      if(data.status_code == 200){

        this.changepassMessage = data.message
        this.resetpassLoader = false;
  this.changepassRes= true;
  this.changePassword.reset();
  setTimeout(() => {
    this.router.navigate(['/']);
  }, 2000);
      }else{
        this.resetpassLoader = false;
        this.changepassMessage = data.error
        this.changepassRes= false;
      }
      })
      }
  showPassword(e:any, number:any){
    var showpassword2 = <HTMLInputElement> document.getElementById("showpassword2");
    var showpassword3 = <HTMLInputElement> document.getElementById("showpassword3");
   if(number == 2){
      if(e.target.checked){
        this.show= 2;
        // showpassword1.checked = false;
        showpassword3.checked = false;
      }else{
        this.show= 0;
      }
    }else if(number ==3){
      if(e.target.checked){
        showpassword2.checked = false;
        // showpassword1.checked = false;
        this.show= 3;
      }else{
        this.show= 0;
      }
    }


  }
  signOut(){
    localStorage.removeItem("user");
    localStorage.removeItem("userData");
    localStorage.removeItem("orderProduct");
    this.dataservice.productItem();
    // localStorage.removeItem("savedPickupLocation");
    this.dataservice.changeMessage("logedOut");
    this.dataservice.logincheckfunction(false);
    document.body.classList.remove('successshow')
  }

}
