import { Component, ElementRef, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, NgForm, Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as $ from "jquery";
import { Observable } from 'rxjs';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { DataService } from 'src/app/services/data.service';
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  signupArray:any = [];
  days:any = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
  show:any = false;
  clubCard: any = false;
  signupshow:any = true;
  cardNeeded:any = true;
  cardHaved:any = false;
  years:any = []
  signupForm: FormGroup;
  needCardForm: FormGroup;
  otpform: FormGroup;
  emailId:any;
  errorMsg:any;
  cardStatus:any;
  tooshort:boolean = false
  weak:boolean = false;
  strong:boolean = false;
  successfulSection:boolean = false;
  signupformdiv:boolean = true;
  otpmain:boolean = false;
  maxTime: any=60;
  timer:any;
  lessthanten:boolean = false;
  resendotpbtn:boolean = false;
  timershow:boolean = false;
  signspin:boolean = false;
  otpspin:boolean =false;
  downloadTimer:any;
  constructor(private formbuilder:FormBuilder, private apiService:ApiServiceService,private router: Router, private dataservice: DataService, private elRef:ElementRef) { 
    this.signupForm = formbuilder.group({
      // avatar: [''],
      fname: ['', [Validators.required]],
      lname: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email, Validators.pattern("[A-Za-z0-9.%-]+@[A-Za-z0-9.%-]+\\.[A-a-Z-z]{2,3}")]],
      password: ['', [Validators.required, Validators.minLength(7), Validators.pattern("^(?=.*?[A-Za-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{7,}$")]],
      homeMobile: ['home', [Validators.required]],
      phone: ['', [Validators.required, Validators.minLength(10), Validators.pattern("^[0-9]*$")]],
      extension: ['',[Validators.pattern("^[0-9]*$"),Validators.minLength(5)]],
      day: [''],
      month: [''],
      year: [''],
      promotions: ['', [Validators.required]],
      language: ['english']
    });
    this.otpform= formbuilder.group({
      otp: ['', [Validators.required, Validators.minLength(6),Validators.maxLength(6), Validators.pattern("^[0-9]*$")]],
    })
this.needCardForm = formbuilder.group({

})

  }

  ngOnInit(): void {
    jQuery(".otpmain").hide();
    if(localStorage.getItem("userData")){
      this.router.navigate(['/']);
     }
    this.dataservice.cartshow(false);
    this.scripts();
    var max = new Date().getFullYear()
    var min = 1922
    
  
    for (var i = max; i >= min; i--) {
      this.years.push(i)
    }
    
  }

  showPassSugg(event:any){
    var e = event.length;
    if(e>=1 && e<7){
      this.tooshort= true;
      this.weak = false;
      this.strong=false;
     
    }else if(e>=7 && e<13){
      this.tooshort= false;
      this.weak = true;
      this.strong=false;
     
    }else if(e>=13){
      this.tooshort= false;
      this.weak = false;
      this.strong=true;
    
    }else{
      this.tooshort= false;
      this.weak = false;
      this.strong=false;
    
    }
  }

  showPassword(e:any){
  
    if(e.target.checked){
      this.show= true;
    }else{
      this.show= false;
    }
    
  }

  formData: FormData = new FormData();
  url="./assets/images/profileDefault.jpg";
  onselectFile(e:any){
    if(e.target.files){
      var reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      this.formData.set("avatar",  e.target.files[0]);

      reader.onload=(event:any)=>{
        this.url=event.target.result;
      }
      // this.signupForm.patchValue({
      //   avatar: e.target.files[0]
      // });
    }
  }

  clearPhotos(){
    var clearfileEvent = <HTMLInputElement> document.getElementById("fileUpload")
   clearfileEvent.value = "";
    this.url="./assets/images/profileDefault.jpg";
    this.formData.set("avatar",  '');
  }


  backotp(){
    console.log(this.signupForm.get('promotions')?.value)
    if(this.signupForm.get('promotions')?.value == "yes"){
      jQuery(".language-box").show();
    }else{
      jQuery(".language-box").hide();
    }
    this.otpspin = false;
    jQuery(".signupformdiv").show();
    jQuery(".otpmain").hide();
    this.signupformdiv = true;
        this.otpmain = false;
  }

  signupPostData(){
    this.resendotpbtn = false; 
    this.signspin = true;
    var otpdata={
      "name":this.signupForm.get('fname')?.value +" "+ this.signupForm.get('lname')?.value,
      "email":this.signupForm.get('email')?.value,
      "phone_number":this.signupForm.get('phone')?.value
  }
  this.otpform.reset();
  this.timershow = true;
  this.maxTime = 60;
  this.StartTimer();

    this.apiService.otp(otpdata).subscribe(data=>{
      this.signspin = false;
      if(data.status_code == 200){
        this.signupformdiv = false;
        this.otpmain = true;
        jQuery(".signupformdiv").hide();
        jQuery(".otpmain").show();
        
        this.lessthanten = false;
        this.resendotpbtn = false; 
        
        
      }else if(data.status_code == 203){
        this.errorMsg = data.message;
        setTimeout(() => {
          this.errorMsg = "";
        }, 5000);
      }
    })
  }

  StartTimer(){
    clearInterval(this.downloadTimer);
    this.lessthanten = false;
    var timeleft = 60;
    this.downloadTimer = setInterval(()=>{
      if(timeleft <= 0){
        clearInterval(this.downloadTimer);
        this.resendotpbtn = true; 
      }
      if(timeleft<10 ){
        this.lessthanten = true;
      }
      this.maxTime = timeleft
      timeleft -= 1;
    }, 1000);


    // this.timer = setTimeout(x => 
      
    //   {
    //     this.lessthanten = false;
    //       // if(this.maxTime <= 0) { }
    //       this.maxTime -= 1;
    //        console.log(this.maxTime);
    //       if(this.maxTime>0){
            
    //         this.StartTimer();
    //       } else if(this.maxTime==0){
    //         this.maxTime = 0;
    //         this.resendotpbtn = true;  
    //       }

    //       if(this.maxTime<10 ){
    //         this.lessthanten = true;
    //       }
          

    //   }, 1000);


  }

afterotpsuccess(otpform:any){
  this.otpspin = true;
  this.formData.append("email",  this.signupForm.get('email')?.value);
  this.formData.append("password",  this.signupForm.get('password')?.value);
  this.formData.append("first_name",  this.signupForm.get('fname')?.value);
  this.formData.append("last_name",  this.signupForm.get('lname')?.value);
  this.formData.append("home_mobile",  this.signupForm.get('homeMobile')?.value);
  this.formData.append("promotion",  this.signupForm.get('promotions')?.value);
  this.formData.append("language",  this.signupForm.get('language')?.value);
  this.formData.append("day",  this.signupForm.get('day')?.value);
  this.formData.append("month",  this.signupForm.get('month')?.value);
  this.formData.append("year",  this.signupForm.get('year')?.value);
  this.formData.append("phone_number",  this.signupForm.get('phone')?.value);
  this.formData.append("extension",  this.signupForm.get('extension')?.value);
  this.formData.append("otp",  otpform.value.otp);

  this.apiService.signup(this.formData).subscribe(data=>{
    if(data.status_code == 200){
      this.otpspin = false;
      this.emailId = data.data.email;
      this.successfulSection = true;
      this.signupformdiv = false;
      this.otpmain = false;
      jQuery(".signupformdiv").hide();
    jQuery(".otpmain").hide();
      let logindata;
      if(localStorage.getItem("session_id")){
        logindata ={"email":this.signupForm.get('email')?.value, "password":this.signupForm.get('password')?.value,  "session_id": JSON.parse(localStorage.getItem("session_id") || "null")}
      }else{
        logindata ={"email":this.signupForm.get('email')?.value, "password":this.signupForm.get('password')?.value,  "session_id": "null"}
  
      }
      this.apiService.login(logindata).subscribe(logindata=>{
       
       if(logindata.status_code == 200){
         localStorage.setItem('user', JSON.stringify(logindata));
         localStorage.setItem('userData', JSON.stringify(logindata.user));
         this.dataservice.changeMessage("logedIn");
         this.dataservice.logincheckfunction(true);
         this.addressList(logindata.user)
         
         this.dataservice.productItem();
        
       }else{
        this.otpspin = false;
        this.errorMsg = data.error
       }
      },
      error =>{
        this.otpspin = false;
    
      })

    }else if(data.status_code == 203){
      this.otpspin = false;
      this.errorMsg = data.error;
      setTimeout(() => {
        this.errorMsg = "";
      }, 5000);
    }
  });
}

addressList(userData:any){
  this.apiService.listUSerPickUp(userData.id).subscribe(list=>{
    var addressData = [];
    if(list.status_code == 200 && list.data.length > 0){
      addressData = list.data.filter((data:any)=>{
          return data.id != JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").data.id
      })
      addressData.push(JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").data) 
      const updatedStoreList = addressData.map((obj:any) =>
obj.id === JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").data.id ? { ...obj, by_default: 1 } : obj
);
localStorage.setItem('savedPickupLocation', JSON.stringify(updatedStoreList));

    }else{
      
      addressData.push(
        JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").data
      )
      addressData.map((obj:any)=>{
        obj.by_default = 1;
      })
      localStorage.setItem('savedPickupLocation', JSON.stringify(addressData));
    }
  })
}
  signupSuccess(){
    
    this.router.navigate(['/']);
    // setTimeout(() => {
    //   window.location.reload();
    //  }, 1000);
    this.successfulSection = false;
        this.signupformdiv = true;
        jQuery(".signupformdiv").show();
   
  }
  chardChange(e:any){
 
    this.cardStatus = e.target.value;
    if(e.target.value == "needcard"){
      this.cardNeeded = true;
      this.cardHaved = false;
    }else if(e.target.value == "havecard"){
      this.cardNeeded = false;
      this.cardHaved = true;
    }
  }

  neddCardPost(needCardData:any){

  }
  scripts(){
    
    $(document).ready(function(){
      jQuery(".yesradio").on('click', function() {
        jQuery(".language-box").show();
      });
      jQuery(".noradio").on('click', function() {
        jQuery(".language-box").hide();
      });
    });
  }
}
