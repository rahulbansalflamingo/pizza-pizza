import { Component, EventEmitter, Injectable, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, NgForm, Validator, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { DataService } from 'src/app/services/data.service';
import { ConfirmPasswordValidator,expirymonthValidator, expiryyearValidator} from 'src/app/helper/confirmed.validators'
import { Location } from '@angular/common';
@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  Paymentmore:boolean =true;
  Paymentuser:boolean = true;
  Addressesmore:boolean =true;
  Addressesuser:boolean = true;
  PaymentShown: boolean = false ;
  AddressesShown: boolean = false;
  imgUrl:any = this.apiService.baseUrl;
  localStorageData:any
  userdata:any
  url:any
  show:any = 0;
  editProfileForm:FormGroup;
  changePassword:FormGroup;
  savecardDetails:FormGroup;
  days:any = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
  years:any = []
  successMessage:any
  changepassMessage:any;
  changepassRes: boolean = false;
  pickupPopupLocation:boolean = false;
  showPickupStore:any;
  deletePickupItemID:any;
  deletePickupItemValue:any;
  seeMenu:boolean= false;
  orderHistory:boolean = false;
  paymentaddesSuccessMessage:any;
  paymentmessageflag:any;
  cardDetailshow:boolean=false;
  paysaveloader:boolean = false;
  cardlistdetails:any=[];
  cardsavedId:any;
  updateprofileLoader:boolean=false;
  constructor(private apiService:ApiServiceService,private location: Location, private router: Router, private dataservice: DataService, private formbuilder:FormBuilder) {
this.editProfileForm = formbuilder.group({
      fname: ['', [Validators.required]],
      lname: ['', [Validators.required]],
      homeMobile: ['', [Validators.required]],
      // phone: ['', [Validators.required, Validators.minLength(10), Validators.pattern("^[0-9]*$")]],
      extension: ['',[Validators.pattern("^[0-9]*$"),Validators.maxLength(5)]],
      day: [''],
      month: [''],
      year: [''],
      promotions: ['', [Validators.required]],
      language: ['english']
});
this.changePassword = formbuilder.group({
  currentPassword: ['',[Validators.required, Validators.minLength(7)]],
  newPassword: ['',[Validators.required, Validators.minLength(7), Validators.pattern("^(?=.*?[A-Za-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{7,}$")]],
  confirmPassword: ['',[Validators.required, Validators.minLength(7)]],
},
      {
        validator: ConfirmPasswordValidator("newPassword", "confirmPassword")
      })

      this.savecardDetails =formbuilder.group({
        cardname:['',[Validators.required]],
        cardnumber:['',[Validators.required,Validators.minLength(15),Validators.maxLength(16),Validators.pattern("^[0-9]*$")]],
        cvv:['',[Validators.required,Validators.minLength(3),Validators.maxLength(4),Validators.pattern("^[0-9]*$")]],
      expiry_month:['',[Validators.required,expirymonthValidator(),Validators.minLength(2),Validators.maxLength(2),Validators.pattern("^[0-9*/]+$")]],
    expiry_year:['',[Validators.required,expiryyearValidator(),Validators.minLength(2),Validators.maxLength(2),Validators.pattern("^[0-9*/]+$")]],
      });

   }
   get f(){
     return  this.changePassword.controls;
   }
  ngOnInit(): void {
    this.location.subscribe((event) => {
      document.body.classList.remove('edit-open');
      // this.router.navigate(['/user/account']);
    });
    if(!localStorage.getItem("userData")){
      this.router.navigate(['/']);
     }
    this.apiService.orderList(JSON.parse(localStorage.getItem("userData")||"null").id).subscribe(data=>{
      if(data.status_code== 200){
        if(data.data.length>0){
          this.seeMenu= false;
          this.orderHistory = true;
        }else{
          this.seeMenu = true;
          this.orderHistory = false;
        }
      }else if(data.status_code== 401){
        this.signOut();
      }
     
    })
    this.dataservice.cartshow(true);
    this.scripts();
    var max = new Date().getFullYear()
    var min = 1922


    for (var i = max; i >= min; i--) {
      this.years.push(i)
    }

    if(!localStorage.getItem("userData")){
      this.router.navigate(['/']);

     }else{

       this.localStorageData = localStorage.getItem("userData");
       this.userdata =JSON.parse(this.localStorageData);
       console.log(this.userdata.phone_number)
       this.url =this.userdata.social_avatar== null ? this.apiService.baseUrl+this.userdata.avatar : this.userdata.social_avatar
       
       this.editProfileForm.patchValue({

         fname: this.userdata.first_name,
         lname: this.userdata.last_name,
        homeMobile: this.userdata.home_mobile,
      //  phone: this.userdata.phone_number,
       extension: this.userdata.extension === "null"? '': this.userdata.extension,
      day: this.userdata.dob === null?'':this.userdata.day,
      month: this.userdata.dob === null?'':this.userdata.month,
      year: this.userdata.dob === null?'':this.userdata.year,
      promotions: this.userdata.promotion,
      language: this.userdata.language
       })
     }
    this.dataservice.currantMessage.subscribe(data=>{
      if(data == "logedIn"){
        if(!localStorage.getItem("user")){
          this.router.navigate(['/']);

         }else{

           this.localStorageData = localStorage.getItem("userData");
           this.userdata =JSON.parse(this.localStorageData);
           this.url =this.userdata.social_avatar== null ? this.apiService.baseUrl+this.userdata.avatar : this.userdata.social_avatar
           this.editProfileForm.patchValue({
             fname: this.userdata.first_name,
             lname: this.userdata.last_name,
            homeMobile: this.userdata.home_mobile,
          //  phone: this.userdata.phone_number,
           extension: this.userdata.extension === "null"? '': this.userdata.extension,
      //      day: this.userdata.dob === "null"?'':this.userdata.day,
      // month: this.userdata.dob === "null"?'':this.userdata.month,
      // year: this.userdata.dob === "null"?'':this.userdata.year,
          promotions: this.userdata.promotion,
          language: this.userdata.language
           })
         }
      }
    });


    // saved Pickup Address
    // this.dataservice.currantPickDeliv.subscribe(data=>{
    //   console.log(data);
    //   if(data == "notSet"){
    //     if(localStorage.getItem("pickupDeliverystores")){
    //       this.showPickupStore = JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").data;
    //       console.log(this.showPickupStore);
    //     }else{
    //       this.pickupPopupLocation = false;
    //     }

    //   }else if(data == "SetPickup"){
    //     this.showPickupStore = JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").data;
    //       console.log(this.showPickupStore);
    //   }
    // });

    this.dataservice.currantsavedAddress.subscribe(data=>{
      if(data == null){
        this.apiService.listUSerPickUp(JSON.parse(localStorage.getItem("userData")||"null").id).subscribe(list=>{
      

          if(list.status_code == 200 && list.data.length > 0){
            this.showPickupStore = list.data
            this.pickupPopupLocation = true;
          }else{
            this.pickupPopupLocation = false;
          }

        })
      }else{
        if(data.status_code == 200 && data.data.length > 0){
          this.showPickupStore = data.data
          this.pickupPopupLocation = true;
        }else{
          this.pickupPopupLocation = false;
        }
      }
    })

this.cardlist();

  }

  //  addHyphen(element:any) {
  //   const input = element.target as HTMLInputElement;
  //   var inputValue = element.target.value;
  //   inputValue = inputValue.split('/').join('');
  //    let finalVal = inputValue.match(/.{1,2}/g).join('/');
  //    input.value = finalVal;
  // }



  // cave card details function
  savecardsubmit(cardDetails:any){
    this.paysaveloader = true;
var cardData= {
  "user_id" : JSON.parse(localStorage.getItem("userData") || "").id,
  "payment_method" : "card",
  "name_on_card" : cardDetails.value.cardname,
  "card_number" : cardDetails.value.cardnumber,
  "cvv" : cardDetails.value.cvv,
  "expiry" :cardDetails.value.expiry_month+"/"+cardDetails.value.expiry_year,
}
this.apiService.addpaymentCard(cardData).subscribe(data=>{
  if(data.status_code==200){
    this.cardlist();
    this.Paymentclick()
    this.paymentaddesSuccessMessage = data.message;
    this.paymentmessageflag = 1;
    this.paysaveloader = false;
    this.savecardDetails.reset();
    
    
    setTimeout(() => {
      this.paymentaddesSuccessMessage = "";
    }, 5000);
   
  }else if(data.status_code==401){
this.signOut();
this.paymentaddesSuccessMessage = data.message;
    this.paymentmessageflag = 0
    this.paysaveloader = false;
  } else{
    this.paymentaddesSuccessMessage = data.message;
    this.paymentmessageflag = 0
    this.paysaveloader = false;
  }
}, error=>{
  console.log(error)
})

  }

  // list card details
  cardlist(){
    var cardData={
      "token":this.apiService.ourToken,
      "user_id":JSON.parse(localStorage.getItem("userData")||"null").id
      
  }
    this.apiService.cardList(cardData).subscribe( async data=>{
      if(data.status_code==200){
        if(data.data.length>0){
          this.cardlistdetails=[];
        await data.data.forEach((element:any) => {
          this.cardlistdetails.push(
           {
            "id":element.id,
            "last_digits":atob(element.last_digits),
            "name_on_card":element.name_on_card,
            "token_info":element.token_info
           } 
          )
         });
          this.cardDetailshow = true;

        }else{
          this.cardDetailshow = false
        }
      }else{

      }
    })
  }
  // delete card details
  removecard(id:any){
    document.body.classList.add('deleteSavedcard');
    this.cardsavedId=id

  }
  deletecardpopup(){
this.apiService.deletecard(this.cardsavedId).subscribe(data=>{
  if(data.status_code==200){
    document.body.classList.remove('deleteSavedcard');
    this.cardlist();
  }
})
  }

  setDefaultList(index:any, userId:any){
  
    var defaultArray = {
      "user_id":userId,
      "location_id":index
  }
  this.apiService.setDefaultPickup(defaultArray).subscribe(data=>{
    if(data.status_code == 200){
      this.dataservice.listAddress();
    }
  })
  }
  /*-- radio-tab-start --*/
  id:any = "creditcard";
  tabChange(ids:any){
    this.id = ids;
  }
  /*-- radio-tab-end --*/
  /*-- Paymentclick-start --*/
  Paymentclick():void{
    this.Paymentmore =!this.Paymentmore;
    this.PaymentShown = ! this.PaymentShown;
    if (this.Paymentuser) {
      this.Paymentuser = false;
      setTimeout(() => {
        var container:any = <HTMLInputElement>document.getElementsByClassName("expiryvalidated")[0];
        container.onkeyup = function(e:any) {
          var target = e.srcElement || e.target;
          var maxLength = parseInt(target.attributes["maxlength"].value, 10);
          var myLength = target.value.length;
          if (myLength >= maxLength) {
              var next = target;
              while (next = next.nextElementSibling) {
                  if (next == null)
                      break;
                  if (next.tagName.toLowerCase() === "input") {
                      next.focus();
                      break;
                  }
              }
          }
          // Move to previous field if empty (user pressed backspace)
          else if (myLength === 0) {
              var previous = target;
              while (previous = previous.previousElementSibling) {
                  if (previous == null)
                      break;
                  if (previous.tagName.toLowerCase() === "input") {
                      previous.focus();
                      break;
                  }
              }
          }
        }
       }, 100);
    } else {
     
      this.Paymentuser = true;
      
    }
  }
  /*-- Paymentclick-end --*/
  /*-- Addressesclick-start --*/
  Addressesclick():void{
    this.Addressesmore =!this.Addressesmore;
    this.AddressesShown = ! this.AddressesShown;
    if (this.Addressesuser) {
      this.Addressesuser = false;
    } else {
      this.Addressesuser = true;
    }
  }
  /*-- Addressesclick-end --*/
  /*-- account-pagepopup-start --*/
  RegisterPopUpShow(): void {
      document.body.classList.add('register-open')
  }
  RegisterPopUpHide(): void {
    document.body.classList.remove('register-open')
  }

  EditPopUpShow(): void {
    document.body.classList.add('edit-open')
  }
  EditPopUpHide(): void {
    this.successMessage="";
    document.body.classList.remove('edit-open')
  }

  PasswordPopUpShow(): void {
    document.body.classList.add('password-open')
  }
  PasswordPopUpHide(): void {
    this.changepassMessage = "";
    document.body.classList.remove('password-open')
  }
  FindsPopUpShow(): void {
    document.body.classList.add('Find-open')
  }
  FindsPopUpHide(): void {
    document.body.classList.remove('Find-open')
  }

  deleteStoreShow(itemId:any, value:any):void{
 
    this.deletePickupItemID = itemId;
    this.deletePickupItemValue = value
    document.body.classList.add('deleteSavedStores')
  }
  deletepopuClose():void{
    document.body.classList.remove('deleteSavedStores')
  }
  deletecardclose():void{
    document.body.classList.remove('deleteSavedcard')
  }


  deletlistPickup(){

this.apiService.deletePickupApi(this.deletePickupItemID).subscribe(data=>{

  this.dataservice.listAddress();
  document.body.classList.remove('deleteSavedStores')
})

var arraycheckoutAddr = JSON.parse(localStorage.getItem("savedPickupLocation") || "null");

arraycheckoutAddr = arraycheckoutAddr.filter((items:any)=>{
  return items.id !== this.deletePickupItemValue.id
})
localStorage.setItem("savedPickupLocation", JSON.stringify(arraycheckoutAddr));
  }
  /*-- account-pagepopup-end --*/
  /*-- profile-start --*/
  formData: FormData = new FormData();
  onselectFile(e:any){
   
    if(e.target.files){
      
      var reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      this.formData.append("avatar",  e.target.files[0]);
      reader.onload=(event:any)=>{
        this.url=event.target.result;
      }
     
    }
  }
  clearfunction(){
   var clearfileEvent = <HTMLInputElement> document.getElementById("fileUpload")
   clearfileEvent.value = "";
    this.url = this.apiService.baseWbesiteUrl+"assets/images/users/default.png";
    this.formData.append("avatar",  "null");
  }
  showPassword(e:any, number:any){
    var showpassword1 = <HTMLInputElement> document.getElementById("showpassword1");
    var showpassword2 = <HTMLInputElement> document.getElementById("showpassword2");
    var showpassword3 = <HTMLInputElement> document.getElementById("showpassword3");
  
    if(number == 1){
      if(e.target.checked){
        this.show= 1;
        showpassword2.checked = false;
        showpassword3.checked = false;

      }else{
        this.show= 0;
      }
    }else if(number == 2){
      if(e.target.checked){
        this.show= 2;
        showpassword1.checked = false;
        showpassword3.checked = false;
      }else{
        this.show= 0;
      }
    }else if(number ==3){
      if(e.target.checked){
        showpassword2.checked = false;
        showpassword1.checked = false;
        this.show= 3;
      }else{
        this.show= 0;
      }
    }


  }

  signupPostData(formdata:any){
    this.updateprofileLoader = true;

    this.formData.append("first_name",  this.editProfileForm.get('fname')?.value);
    this.formData.append("last_name",  this.editProfileForm.get('lname')?.value);
    this.formData.append("home_mobile",  this.editProfileForm.get('homeMobile')?.value);
    this.formData.append("promotion",  this.editProfileForm.get('promotions')?.value);
    this.formData.append("language",  this.editProfileForm.get('language')?.value);
    this.formData.append("day",  this.editProfileForm.get('day')?.value);
    this.formData.append("month",  this.editProfileForm.get('month')?.value);
    this.formData.append("year",  this.editProfileForm.get('year')?.value);
    // this.formData.append("phone_number",  this.editProfileForm.get('phone')?.value);
    this.formData.append("extension",  this.editProfileForm.get('extension')?.value);
    this.formData.append("id", this.userdata.id);
    console.log(this.formData)
    this.apiService.editprofile(this.formData).subscribe(data=>{
      this.updateprofileLoader = false;
      console.log(data)
      localStorage.setItem('userData', JSON.stringify(data.data));
        this.dataservice.changeMessage("logedIn");
        this.successMessage = data.message;
        

    })

  }
  updatePassword(changePaword:any){

var updatepassArray = {
  "user_id":this.userdata.id,
  "old_password":this.changePassword.get("currentPassword")?.value,
  "new_password": this.changePassword.get("newPassword")?.value
}
this.apiService.updatePassword(updatepassArray).subscribe(data=>{

  if(data.status_code == 200){
    this.changepassMessage = data.message
  this.changepassRes= true;
  this.changePassword.reset();
  this.successshow()
  }else if(data.status_code == 203){
    this.changepassMessage = data.error
  this.changepassRes= false;
  }
})

  }

  /*-- profile-end --*/

  // scripts(){

  //   $(document).ready(function(){
  //     jQuery(".yesradio").on('click', function() {
  //       jQuery(".language-box").show();
  //     });
  //     jQuery(".noradio").on('click', function() {
  //       jQuery(".language-box").hide();
  //     });
  //   });
  // }
  signOut(){
    localStorage.removeItem("user");
    localStorage.removeItem("userData");
    localStorage.removeItem("orderProduct");
    this.dataservice.productItem();
    // localStorage.removeItem("savedPickupLocation");
    this.dataservice.changeMessage("logedOut");
    this.router.navigate(['/']);
    this.dataservice.logincheckfunction(false);
    document.body.classList.remove('successshow')
  }
  successshow():void{
    document.body.classList.remove('password-open')
    document.body.classList.add('successshow')
  }
  successClose():void{
    document.body.classList.remove('successshow')
  }
  scripts(){

    $(document).ready(function(){
      jQuery(".yesradio").on('click', function() {
        jQuery(".language-box").show();
      });
      jQuery(".noradio").on('click', function() {
        jQuery(".language-box").hide();
      });
      jQuery("#findpopup2").on('click', function(){
        jQuery(".find-popup").addClass("is-visible");
      });
      jQuery(".size-close").on('click', function(){
        jQuery(".find-popup").removeClass("is-visible");
      });
      jQuery('.findpopuptab li a').on('click', function(){
        var target = $(this).attr('data-tab');
        jQuery('.findpopuptab li a').removeClass('active');
        jQuery(this).addClass('active');
        jQuery("#"+target).fadeIn('slow').siblings(".tab-pane").hide();
        return false;
      });
    });
  }
}
