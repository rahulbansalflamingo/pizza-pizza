import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { CatalogRoutingModule } from './catalog-routing.module';
import { CheckoutGuestComponent } from './checkout-guest/checkout-guest.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { PizzaAssistantComponent } from './pizza-assistant/pizza-assistant.component';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
// import { SocialLoginModule, SocialAuthServiceConfig } from '@abacritt/angularx-social-login';
import { HelperModule } from '../helper/helper.module';
// import {
//   GoogleLoginProvider,
//   FacebookLoginProvider
// } from '@abacritt/angularx-social-login';
import { WebLoaderModule } from '../web-loader/web-loader.module';


@NgModule({
  declarations: [
    CheckoutGuestComponent,
    CheckoutComponent,
    PizzaAssistantComponent
  ],
  imports: [
    CommonModule,
    CatalogRoutingModule,
    SlickCarouselModule,
    ReactiveFormsModule,
    FormsModule,
    // SocialLoginModule,
    WebLoaderModule,
    HelperModule
  ],
  // providers: [
    
  //   {
  //     provide: 'SocialAuthServiceConfig',
  //     useValue: {
  //       autoLogin: false,
  //       providers: [
  //         {
  //           id: GoogleLoginProvider.PROVIDER_ID,
  //           provider: new GoogleLoginProvider(
  //             '297058395849-11o9fhmflj8ge74i66vosmc8fa8fpmcr.apps.googleusercontent.com',
  //             {
  //               scopes:'email',
                
  //             }
  //           )
  //         },
  //         {
  //           id: FacebookLoginProvider.PROVIDER_ID,
  //           provider: new FacebookLoginProvider('388770269874586')
  //         }
  //       ],
  //       onError: (err) => {
  //         console.error(err);
  //       }
  //     } as SocialAuthServiceConfig,
  //   }
  // ],
})
export class CatalogModule { }
