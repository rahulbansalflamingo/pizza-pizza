import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PizzaAssistantComponent } from './pizza-assistant.component';

describe('PizzaAssistantComponent', () => {
  let component: PizzaAssistantComponent;
  let fixture: ComponentFixture<PizzaAssistantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PizzaAssistantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PizzaAssistantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
