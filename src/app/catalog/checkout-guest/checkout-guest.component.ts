import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, NgForm, Validator, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { DataService } from 'src/app/services/data.service';
// import { SocialAuthService } from '@abacritt/angularx-social-login';
// import { FacebookLoginProvider, GoogleLoginProvider } from '@abacritt/angularx-social-login';
// import { SocialUser } from "@abacritt/angularx-social-login";


@Component({
  selector: 'app-checkout-guest',
  templateUrl: './checkout-guest.component.html',
  styleUrls: ['./checkout-guest.component.scss']
})
export class CheckoutGuestComponent implements OnInit {
  wholepage:boolean=false;
  wholeloader:boolean = false;
  array:any =[];
  show:any = false;
  loginForm: FormGroup;
  loginError: any;
  socialloginError:any;
  signText: boolean = true;
  loader:boolean = false;
  fbLoader:boolean = false;
  ggLoader:boolean = false;
  forgotSuccessMessage:any;
  forgotmessageflag:any
  forgotloader:boolean = false;
  forgotpassword:FormGroup;
  auth2:any;
  forgetcontinueLink:boolean = false;
  forgetpasswordlink:boolean = true;
  cartProduct:any
  // public user: any;
  public loggedIn!: boolean;
  constructor(
    private router: Router,
    private dataservice: DataService,
    private formbuilder:FormBuilder,
    private apiService:ApiServiceService,
    // private authService: SocialAuthService
  ) {
    this.loginForm = formbuilder.group({
      emailId: ['', [Validators.required, Validators.email]],
      userPassword: ['', [Validators.required, Validators.minLength(7)]]
    })
    this.forgotpassword = formbuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.pattern("[A-Za-z0-9.%-]+@[A-Za-z0-9.%-]+\\.[A-a-Z-z]{2,3}")]]
    })
   }

  ngOnInit(): void {
    this.wholeloader = true;
    this.dataservice.cartshow(false);
    // if(localStorage.getItem("userData")){
    //   var userID:any = JSON.parse(localStorage.getItem("userData") || "null").id;
    //   var sessionId :any=null
    //  }else if(localStorage.getItem("session_id")){
      //  var userID:any = null;
      // var sessionId :any=JSON.parse(localStorage.getItem("session_id") || "null")
    //  }
    var storeid;
    if(localStorage.getItem("pickupDeliverystores")){
     storeid =JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").data.id;
    }else{
     storeid = null
    }
     this.apiService.listcart(null, JSON.parse(localStorage.getItem("session_id") || "null"), storeid).subscribe((data:any)=>{
      this.wholepage = true;
      this.wholeloader = false;
      if(data.data.length == 0){
        this.router.navigate(['/'])
      }else{
        this.cartProduct = data.data;
        
      }
     
    })
      // this.dataservice.currantorderProductLo.subscribe(data =>{
      //   if(data == null){
      //     console.log("patel");
          
        
      //   }else{
      //     this.cartProduct = data.data;
      //   }
      // })
      

  }
  showPassword(e:any){
    if(e.target.checked){
      this.show= true;
    }else{
      this.show= false;
    }
    
  }
   // forgot password function
   forgotpasswordfunction(inputdata:any){
    this.forgotloader=true;
var forgotemail = {
  "auth_user_email":inputdata.value.email
}
this.apiService.forgotpassword(forgotemail).subscribe(data=>{
  if(data.status_code == 200){
    this.forgotSuccessMessage = data.message;
    this.forgotmessageflag = 1
    this.forgotloader=false;
    this.forgotpassword.reset();
    this.forgetcontinueLink = true;
  this.forgetpasswordlink = false;
    setTimeout(() => {
      this.forgotSuccessMessage = '';
    }, 5000);
  }else{
    this.forgotSuccessMessage = data.error;
    this.forgotmessageflag = 0
    this.forgotloader=false;
  }
})

  }
  guestCheckout(){
    localStorage.setItem("guestCheckout", JSON.stringify(1));
    this.router.navigate(['/catalog/checkout']);
    // setTimeout(() => {
    //   window.location.reload();
    //  }, 100);
  }
  PasswordPopUpShow(): void {
    document.body.classList.add('password-open')
  }
  PasswordPopUpHide(): void {
    document.body.classList.remove('password-open')
  }

  postData(loginForm:any){
    if(localStorage.getItem("guestCheckout")){
      localStorage.removeItem("guestCheckout");
    }
    
    this.signText = false;
    this.loader = true;
    var logindata;
    if(localStorage.getItem("session_id")){
      logindata ={"email":this.loginForm.get('emailId')?.value, "password":this.loginForm.get('userPassword')?.value,  "session_id": JSON.parse(localStorage.getItem("session_id") || "null")}
    }else{
      logindata ={"email":this.loginForm.get('emailId')?.value, "password":this.loginForm.get('userPassword')?.value,  "session_id": "null"}

    }
    
     this.apiService.login(logindata).subscribe(data=>{
       this.array = data;
      if(this.array.status_code == 200){
        localStorage.setItem('user', JSON.stringify(data));
        localStorage.setItem('userData', JSON.stringify(data.user));
        this.dataservice.changeMessage("logedIn");
        this.signText = true;
        this.loader = false;
        this.dataservice.productItem();
        this.router.navigate(['/catalog/checkout']);
        // setTimeout(() => {
        //   window.location.reload();
        //  }, 100);
      }else{
        this.signText = true;
        this.loader = false;
        this.loginError = data.error
      }
     },
     error =>{
       console.log(error);
     })
  }
  signInWithFB(): void {
    if(localStorage.getItem("guestCheckout")){
      localStorage.removeItem("guestCheckout");
    }
    this.fbLoader = true;
  //  this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(
  //    d => {
  //      // var abc: any = [];
  //      this.apiService.socialLogin(d).subscribe(data=>{
  //        if(data.status_code == 200){
  //          localStorage.setItem('user', JSON.stringify(data));
  //          localStorage.setItem('userData', JSON.stringify(data.user));
  //          this.dataservice.changeMessage("logedIn");
  //          this.fbLoader = false;
  //          this.router.navigate(['/catalog/checkout']);
  //         //  setTimeout(() => {
  //         //   window.location.reload();
  //         //  }, 100);
  //        }else{
  //          this.fbLoader = false;
           
  //        }
  //      },
  //      error=>{
  //        console.log(error);
  //        this.fbLoader = false;
  //      })

  //    })
  //  .catch(error => {
  //    this.fbLoader = false;
  //    console.log(error);
  //    this.socialloginError = error.error;
  //  });;
 }
}
