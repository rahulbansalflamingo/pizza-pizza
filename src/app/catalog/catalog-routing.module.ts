import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CheckoutGuestComponent } from './checkout-guest/checkout-guest.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { PizzaAssistantComponent } from './pizza-assistant/pizza-assistant.component';
const routes: Routes = [
  { path: 'checkout-guest', component: CheckoutGuestComponent },
  { path: 'checkout', component: CheckoutComponent },
  { path: 'pizza-assistant', component: PizzaAssistantComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogRoutingModule { }
