import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { DataService } from 'src/app/services/data.service';
import {interval, Observable, Subscription} from 'rxjs';
import { expirymonthValidator, expiryyearValidator} from 'src/app/helper/confirmed.validators'
import { json } from 'express';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  loader:boolean = false;
  paymentCheck:boolean=false;
  wholeloader:boolean = false;
  wholePage:boolean = false;
  pickupShown: boolean = true ;
  deliveryShown: boolean = false ;
  forfutureShown: boolean = false ;
  universityShown: boolean = false ;
  deliverytopShown: boolean = false ;
  addressShown: boolean = false ;
  activeIndex = 0;
  applyShown: boolean = false ;
  tipdriverShown: boolean = false ;
  cartProduct:any;
  totalPizzaCost:any;
  lastTotalpriceCost:any
  // gst:any = 2.68;
  // pst:any = 2.68;
  iva:any;
  isDisabled:boolean = true;
  isyorInfo:boolean = true;
  pickupStores:any
  reviewCheck:boolean = false;
  reviewCheckAddress:boolean = false;
  showPickupStore:any;
  withoutsignUpform:boolean = false;
  withsignUpform:boolean = false;
  pickupPopupLocation:boolean = false;
  checkoutLogedIn: FormGroup;
  checkoutguestDetail: FormGroup;
  selectedAddrCheck:boolean = true;
  tabname:any= "pickup";
  pickupTime:any = "fornow";
  storeStatus:boolean = false;
  paymentType:any;
  paymentMethod:any;
  storefilter:any
  removeCartItem:any;
  emptyCartData:boolean =false;
  savecardDetails:FormGroup;
  tokencvv:FormGroup;
  paysaveloader:boolean = false;
  savepaymentmethod:boolean=false;
  errorflagpayonline:boolean=false;
  errormessagepayonline:any;
  myDynamicMarkup:any;
  sub!:Subscription
  paystatus!:Subscription
  paymentformaction:any;
  paymentcreq:any;
  paymentsessiondata:any;
  cardlistdetails:any=[];
  cardDetailshow:boolean=false;
  cvvCard:any ="";
  tokencvvOpen:any;
  popupchrome:boolean =true;
  popupsafari:boolean =false;
  popupflag:boolean = false;
  payatrestro:boolean =true;
  paymentfailed:any
  callbackcount:any=1;
  cartratelooader:boolean = false
  constructor(
    private dataservice: DataService,
    private router: Router,
    private route: ActivatedRoute,
    private apiService:ApiServiceService,
    private formbuilder:FormBuilder,
    private renderer: Renderer2,
    usernameElement: ElementRef
  ) {

    this.checkoutLogedIn = formbuilder.group({
      phone: ['', [Validators.required, Validators.minLength(10), Validators.pattern("^[0-9]*$")]],
      extension: ['',[Validators.pattern("^[0-9]*$"),Validators.maxLength(5)]],
      contactless: [false]
    })
    this.checkoutguestDetail = formbuilder.group({
      fname: ['', [Validators.required]],
      lname: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email, Validators.pattern("[A-Za-z0-9.%-]+@[A-Za-z0-9.%-]+\\.[A-a-Z-z]{2,3}")]],
      phone: ['', [Validators.required, Validators.minLength(10),Validators.maxLength(10), Validators.pattern("^[0-9]*$")]],
      extension: ['',[Validators.pattern("^[0-9]*$"),Validators.maxLength(5)]],
      contactless: [false]
    })
    this.savecardDetails =formbuilder.group({
      cardname:['',[Validators.required]],
      cardnumber:['',[Validators.required,Validators.minLength(15),Validators.maxLength(16),Validators.pattern("^[0-9]*$")]],
      cvv:['',[Validators.required,Validators.minLength(3),Validators.maxLength(4),Validators.pattern("^[0-9]*$")]],
      // expiry:['',[Validators.required,expiryDateValidator(),Validators.minLength(5),Validators.maxLength(5),Validators.pattern("^[0-9*/]+$")]],
      expiry_month:['',[Validators.required,expirymonthValidator(),Validators.minLength(2),Validators.maxLength(2),Validators.pattern("^[0-9*/]+$")]],
      expiry_year:['',[Validators.required,expiryyearValidator(),Validators.minLength(2),Validators.maxLength(2),Validators.pattern("^[0-9*/]+$")]],
    });

    this.tokencvv = formbuilder.group({
      cvv:['',[Validators.required,Validators.minLength(3),Validators.maxLength(4),Validators.pattern("^[0-9]*$")]],
    })
   }


  ngOnInit(): void {
   

    let script = this.renderer.createElement('script');
    script.type = "text/javascript";
    script.src = "assets/js/custom.js";


    
    this.renderer.removeClass(document.body, 'hidden-scroll-body');
    this.route.queryParams
      .subscribe((params:any) => {
        if(params.msg != undefined){
          this.incompleteshow();
           this.paymentfailed = params.msg;
        }
      }
    );
      
   
    if(localStorage.getItem("userData")){
      this.savepaymentmethod = true;
    }
this.wholeloader = true;
    if(!localStorage.getItem("pickupDeliverystores")){
      this.router.navigate(['/'])
    }
    const today = new Date()
const tomorrow = new Date(today)
tomorrow.setDate(tomorrow.getDate() + 7)

    this.dataservice.cartshow(false);
    if(localStorage.getItem("userData") && localStorage.getItem("session_id")){
      var userID:any = JSON.parse(localStorage.getItem("userData") || "null").id;
      var sessionId :any=JSON.parse(localStorage.getItem("session_id") || "null")
    }
    else if(localStorage.getItem("userData")){
      var userID:any = JSON.parse(localStorage.getItem("userData") || "null").id;
      var sessionId :any=null
     }else if(localStorage.getItem("session_id")){
       var userID:any = null;
      var sessionId :any=JSON.parse(localStorage.getItem("session_id") || "null")
     }else{
      var userID:any = null;
      var sessionId :any=null;
     }
     this.dataservice.currantorderProductLo.subscribe(data =>{
      
        if(data == null){
          var storeid;
          if(localStorage.getItem("pickupDeliverystores")){
           storeid =JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").data.id;
          }else{
           storeid = null
          }
          this.apiService.listcart(userID, sessionId, storeid).subscribe((data:any)=>{
            
            if(data.data.length == 0){
              this.emptyCartData= true;
              this.wholePage=false;
              this.wholeloader = false;
            }else{
              this.cartProduct = data.data;
              this.totalPizzaCost = data.data.reduce((accumulator:number, object:any) => {
                return accumulator + parseInt(object.total_amount);
              }, 0);
              this.iva = (this.totalPizzaCost*16)/100
              var taxpcost=this.totalPizzaCost
              
               this.lastTotalpriceCost = parseFloat(taxpcost).toFixed(2);
              this.wholeloader = false;
              this.wholePage= true
            }
            this.cartratelooader = true;
          })
        }else{
          if(data.data.length == 0){
            this.emptyCartData= true;
            this.wholePage=false;
            this.wholeloader = false;
          }else{
            this.cartProduct = data.data;
            this.totalPizzaCost = data.data.reduce((accumulator:number, object:any) => {
                      return accumulator + parseInt(object.total_amount);
                    }, 0);
                    this.iva = (this.totalPizzaCost*16)/100
                    
                    var taxpcost=this.totalPizzaCost
               this.lastTotalpriceCost = parseFloat(taxpcost).toFixed(2);
            this.wholeloader = false;
            this.wholePage= true
          }
          this.cartratelooader = true;
        }

       
     })

    // if(localStorage.getItem("orderProduct")){
    //   this.dataservice.currantorderProductLo.subscribe(data =>{
    //     if(data == null){
    //       this.router.navigate(['/'])
    //     }else{
    //       this.cartProduct = data;

    //       this.totalPizzaCost = data.reduce((accumulator:number, object:any) => {
    //         return accumulator + object.rate;
    //       }, 0);
    //     }

    //   })
    // }else{
    //   this.router.navigate(['/'])
    // }

    // pickup stores
    if(localStorage.getItem("pickupDeliverystores")){
      this.pickupStores = JSON.parse(localStorage.getItem("pickupDeliverystores") || "").data;
    }else{
      this.router.navigate(['/'])
    }

    this.dataservice.checkoutAddressOb.subscribe((data:any)=>{
      this.addressList()
    })

    if(localStorage.getItem("userData")){
      this.withoutsignUpform = false;
      this.withsignUpform = true;
    }else{
      this.withoutsignUpform = true;
      this.withsignUpform = false;
    }


    //address list

    // fill phone number
   if(JSON.parse(localStorage.getItem("userData") || "null")){
    var userdata =JSON.parse(localStorage.getItem("userData") || "null");
    this.checkoutLogedIn.patchValue({
      phone: userdata.phone_number,
      extension: userdata.extension === "null"? '': userdata.extension,
    })
   }
// fill user detail without login
   if(JSON.parse(localStorage.getItem("userformdetail") || "null")){
    var userformdata =JSON.parse(localStorage.getItem("userformdetail") || "null");
    this.checkoutguestDetail.patchValue({
      fname: userformdata.first_name,
      lname: userformdata.last_name,
      email: userformdata.email,
      phone: userformdata.customer_mobile,
      extension: userformdata.customer_extention,
      contactless: userformdata.contactless_pickup
    })
   }
   setTimeout(() => {
    this.storeAvailCheck();
    
   }, 2000);




  //  check outofstock on change stores
  this.dataservice.currantPickDeliv.subscribe(async data=>{
    let addressData = JSON.parse(localStorage.getItem("pickupDeliverystores") ||"");
    if(data == "SetPickup"){
      await this.apiService.outofstock(addressData.data.customer_key).then(outofstockdata=>{

         
      })
    }
  })

  this.cardlist();
//this.cleckpopupblocker();
  }

  changed(evt:any) {
    if(evt.target.checked == true){
      this.payatrestro = false;
    }else if(evt.target.checked == false){
      this.payatrestro = true;
    }
  }

  // cleckpopupblocker(){
    
  //   var windowUrl = 'about:blank';
  //   var windowId = 'TestPopup_' + new Date().getTime();
  //   var windowFeatures = 'left=0,top=0,width=1px,height=1px';
  //   var windowRef = window.open(windowUrl, windowId, windowFeatures);
    
  //   console.log(windowRef);
  //   if ( !windowRef || windowRef.closed || typeof windowRef.closed=='undefined'){
  //     console.log(windowRef);
  //     console.log("popup blocked")
  //     this.popupflag = true;
  //     document.body.classList.add('popupblockeropenclose')
  //     // var browserAgent = checkBrowser();
  //     let userAgent = navigator.userAgent;
  //     let browserName;
      
  //     if(userAgent.match(/chrome|chromium|crios/i)){
  //         browserName = "chrome";
  //         this.popupchrome = true;
  //         this.popupsafari = false;
  //       }else if(userAgent.match(/firefox|fxios/i)){
  //         browserName = "firefox";
  //         this.popupchrome = true;
  //         this.popupsafari = false;
  //       }  else if(userAgent.match(/safari/i)){
  //         browserName = "safari";
  //         this.popupchrome = false;
  //         this.popupsafari = true;
  //       }else if(userAgent.match(/opr\//i)){
  //         browserName = "opera";
  //         this.popupchrome = true;
  //         this.popupsafari = false;
  //       } else if(userAgent.match(/edg/i)){
  //         browserName = "edge";
  //         this.popupchrome = true;
  //         this.popupsafari = false;
  //       }else{
  //         browserName="No browser detection";
  //         this.popupchrome = true;
  //         this.popupsafari = false;
  //       }
  //       console.log(browserName);
  //   }else{
  //     this.popupflag = false;
  //     windowRef.close();
  //     document.body.classList.remove('popupblockeropenclose')
  //     console.log("popup not blocked");
  //       }

  // }
  
  // check store open close
storeAvailCheck(){
 
var savedStoreList = JSON.parse(localStorage.getItem("savedPickupLocation") || "null")
var sectedStoreItem = savedStoreList.filter((item:any)=>{
  return item.by_default == 1
})


const date = new Date();
var newDate = new Date(date.getUTCFullYear(),date.getUTCMonth(),date.getUTCDate(),date.getUTCHours(),date.getUTCMinutes(),date.getUTCSeconds());
var CSToffSet = -360;
var offset= CSToffSet*60*1000;
var CSTTime = new Date(newDate.getTime()+offset);

  //closing hours to minutes
  var closingHours = sectedStoreItem[0].timings[CSTTime.getDay()].closed_time.trim().split(/\s+/);

  var closingMinute;
  var closingHoursSplit = closingHours[0].trim().split(/:/);
  if(closingHours[1]=="PM"){

    closingMinute = (Number(closingHoursSplit[0])+12)*60+Number(closingHoursSplit[1])
  }else{
    closingMinute = (Number(closingHoursSplit[0]))*60+Number(closingHoursSplit[1])
  }

   // opening hours to minutes
   var openingHours = sectedStoreItem[0].timings[CSTTime.getDay()].open_time.trim().split(/\s+/);

   var openingminutes;
   var openingHoursSplit = openingHours[0].trim().split(/:/);
   if(openingHours[1]=="PM"){

  
     openingminutes = (Number(openingHoursSplit[0])+12)*60+Number(openingHoursSplit[1])
   }else{
     openingminutes = (Number(openingHoursSplit[0]))*60+Number(openingHoursSplit[1])
   }

   if(openingminutes <= (CSTTime.getHours()*60+CSTTime.getMinutes()) && (CSTTime.getHours()*60+CSTTime.getMinutes()) <= closingMinute){
   
    this.storeStatus = true;
   }else{
  

    this.storeStatus = false;
     document.body.classList.add('storeCloseSavedStores2')
     localStorage.removeItem("pickupDeliverystores");
   }
  }
  storeClosepopuClose(){
    document.body.classList.remove('storeCloseSavedStores2')
    localStorage.removeItem("orderProduct");
    this.dataservice.productItem();
    document.body.classList.remove('Find-open') 
  }
  // addHyphen(element:any) {
  //   const input = element.target as HTMLInputElement;
  //   if(element.target.value.length>1){
  //     var inputValue = element.target.value;
  //     inputValue = inputValue.split('/').join('');
  //      let finalVal = inputValue.match(/.{1,2}/g).join('/');
      
  //      input.value = finalVal;
  //   }
    
  // }





  contactData(formData:any){

    if (this.idc == "checkoutthree"){
      this.idc = ' ';

    }
    else{
      this.idc= "checkoutthree";
    }
    this.reviewCheckAddress = true;
    var pizzasmheading:any = document.getElementsByClassName('pizzaAddressHead') as HTMLCollectionOf<HTMLElement>
    pizzasmheading[0].style.color="#4e7026"

}
contactGuestData(formData:any){
  if (this.idc == "checkoutthree"){
    this.idc = ' ';

  }
  else{
    this.idc= "checkoutthree";
  }
  this.reviewCheckAddress = true;
  var pizzasmheading:any = document.getElementsByClassName('pizzaAddressHead') as HTMLCollectionOf<HTMLElement>
    pizzasmheading[0].style.color="#4e7026"
   
}
 // list card details
 cardlist(){
if(localStorage.getItem("userData")){
  var cardData={
    "token":this.apiService.ourToken,
    "user_id":JSON.parse(localStorage.getItem("userData")||"null").id
    
}
  this.apiService.cardList(cardData).subscribe( async data=>{

    if(data.status_code==200){
      if(data.data.length>0){
        this.cardlistdetails=[];
      await data.data.forEach((element:any) => {
        this.cardlistdetails.push(
         {
          "id":element.id,
          "last_digits":atob(element.last_digits),
          "name_on_card":element.name_on_card,
          "token_info":element.payment_token
         } 
        )
       });
        this.cardDetailshow = true;

      }else{
        this.cardDetailshow = false;
        this.savepaymentmethod =false;
      }
    }else{

    }
  })
}
}

popupblockerClose(){
  document.body.classList.remove('popupblockeropenclose')
  setTimeout(() => {
    window.location.reload();
   }, 2500);
}
// pay online
  payonlineorder(cardDetails:any){
      this.paysaveloader = true;
    if(localStorage.getItem("userData")){
      var playorderDetails:any={
        "store_id":this.storefilter[0].id,
        "session_id":null,
        "user_id":JSON.parse(localStorage.getItem("userData") || "null").id,
        "payment_method" : "Online Pay",
        "payment_type":"first pay",
      "name_on_card" : cardDetails.value.cardname,
      "card_number" : cardDetails.value.cardnumber,
      "cvv" : cardDetails.value.cvv,
      "expiry" :cardDetails.value.expiry_month+"/"+cardDetails.value.expiry_year,
      "total_order_amount":this.totalPizzaCost,
      "tax_amount":this.iva,
      "amount_to_be_paid":this.lastTotalpriceCost,
      "tabType":JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").type == "Pickup"? "take_out": "delivery",
       "special_instructions":"",
       "delivery_instructions":"",
       "first_name":JSON.parse(localStorage.getItem("userData") || "null").first_name,
       "last_name":JSON.parse(localStorage.getItem("userData") || "null").last_name,
       "email":JSON.parse(localStorage.getItem("userData") || "null").email,
      "customer_mobile":this.checkoutLogedIn.get('phone')?.value,
      "customer_extention":this.checkoutLogedIn.get('extension')?.value,
      "contactless_pickup":this.checkoutLogedIn.get('contactless')?.value,
      }
    }else{
      var playorderDetails:any={
        "store_id":this.storefilter[0].id,
        "user_id":null,
        "session_id":JSON.parse(localStorage.getItem("session_id") || "null"),
        "payment_method" : "Online Pay",
        "payment_type":"first pay",
        "name_on_card" : cardDetails.value.cardname,
        "card_number" : cardDetails.value.cardnumber,
        "cvv" : cardDetails.value.cvv,
        "expiry" :cardDetails.value.expiry_month+"/"+cardDetails.value.expiry_year,
      "total_order_amount":this.totalPizzaCost,
      "tax_amount":this.iva,
      "amount_to_be_paid":this.lastTotalpriceCost,
      "tabType":JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").type == "Pickup"? "take_out": "delivery",
       "special_instructions":"",
       "delivery_instructions":"",
       "first_name":this.checkoutguestDetail.get('fname')?.value,
       "last_name":this.checkoutguestDetail.get('lname')?.value,
       "email":this.checkoutguestDetail.get('email')?.value,
      "customer_mobile":this.checkoutguestDetail.get('phone')?.value,
      "customer_extention":this.checkoutguestDetail.get('extension')?.value,
      "contactless_pickup":this.checkoutguestDetail.get('contactless')?.value,
      }
      localStorage.setItem("userformdetail",JSON.stringify(playorderDetails))
    }


    this.paymentProcess(playorderDetails);
  

    
      }


      selectfortoken(i:any){
        this.tokencvvOpen = i
        this.idp = ' ';
      }
      // payment with token
      paywithToken(cvvform:any, cardDetails:any){
          // var clientRequestId = JSON.parse(atob(cardDetails.token_info));
          if(localStorage.getItem("userData")){
            var playorderDetails:any={
                "payment_token":cardDetails.token_info,
                "cvv":cvvform.value.cvv,
                "store_id":this.storefilter[0].id,
                "session_id":null,
                "user_id":JSON.parse(localStorage.getItem("userData") || "null").id,
                "payment_method" : "Online Pay",
                "payment_type":"first pay",
                "total_order_amount":this.totalPizzaCost,
                "tax_amount":this.iva,
                "amount_to_be_paid":this.lastTotalpriceCost,
                "tabType":JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").type == "Pickup"? "take_out": "delivery",
                "special_instructions":"",
                "delivery_instructions":"",
                "first_name":JSON.parse(localStorage.getItem("userData") || "null").first_name,
                "last_name":JSON.parse(localStorage.getItem("userData") || "null").last_name,
                "email":JSON.parse(localStorage.getItem("userData") || "null").email,
                "customer_mobile":this.checkoutLogedIn.get('phone')?.value,
                "customer_extention":this.checkoutLogedIn.get('extension')?.value,
                "contactless_pickup":this.checkoutLogedIn.get('contactless')?.value,
                "last_digits": cardDetails.last_digits
            }
            this.paymentProcess(playorderDetails);
            this.paysaveloader = true;
          }
  

       
          }

      // payment process

      paymentProcess(playorderDetails:any){
        this.apiService.placeorder(playorderDetails).subscribe(async data=>{
          
          
          if( await data.status_code == 200){
            var orderid:any = {
              "order_id":data.data.order_id
            }
            if(!('responseType' in data.txn_data)){

              if(data.txn_data.transactionStatus == "WAITING"){
                if(data.txn_data.authenticationResponse != undefined){
                 
                  if(data.txn_data.authenticationResponse.secure3dMethod != undefined){
                    var iframecall:any = document.getElementById("iframecall");
                    // console.log(data.txn_data.authenticationResponse.secure3dMethod.methodForm, "secure3dMethod")
                    iframecall.innerHTML= data.txn_data.authenticationResponse.secure3dMethod.methodForm;
                    // console.log(iframecall, "iframecall")
                    
                  }else{

                    if(data.txn_data.authenticationResponse.params != undefined){
                      this.paymentformaction  =await data.txn_data.authenticationResponse.params.acsURL;
                                this.paymentcreq = await data.txn_data.authenticationResponse.params.cReq;
                                setTimeout(() => {
                                  var authenticationsubmit = <HTMLFormElement>document.getElementById('authenticationResponse');
                                authenticationsubmit.submit();
                                this.sub.unsubscribe();
                                this.paymentstatus(orderid, data);
                                }, 100);
                    }else{
                      this.sub = interval(5000)
                      .subscribe(() => { 
                        this.apiService.paymentstatus(orderid).subscribe(async status=>{
                          // console.log(status,"patel");
                         if(status.status_code == 200){
                          if(status.data.status == 0){
                            if(status.data.callback_data != null){
                              var callbackData = await JSON.parse(status.data.callback_data)
                              // console.log(callbackData);
                              if(('responseType' in callbackData)==false){
                                if(callbackData.transactionStatus == "APPROVED"){
                                  this.sub.unsubscribe();
                                  this.paymentstatus(orderid, data);
                                }else if(callbackData.authenticationResponse.params == "undefined"){
                                  this.sub.unsubscribe();
                                this.paysaveloader = false;
                          this.errorflagpayonline=true;
                          this.errormessagepayonline="Payment declined, Please try again";
                          this.incompleteshow();
                          this.paymentfailed = "Payment declined, Please try again";
                                }
                                else{
                                  this.paymentformaction  =await callbackData.authenticationResponse.params.acsURL;
                                  this.paymentcreq = await callbackData.authenticationResponse.params.cReq;
                                  setTimeout(() => {
                                    var authenticationsubmit = <HTMLFormElement>document.getElementById('authenticationResponse');
                                  authenticationsubmit.submit();
                                  this.sub.unsubscribe();
                                  this.paymentstatus(orderid, data);
                                  }, 100);
                                }
                              
                              // this.paysaveloader = false;
                              }else{
                                this.sub.unsubscribe();
                                this.paysaveloader = false;
                          this.errorflagpayonline=true;
                          this.errormessagepayonline="Payment declined, Please try again";
                          this.incompleteshow();
                          this.paymentfailed = "Payment declined, Please try again";
                          setTimeout(() => {
                            this.errorflagpayonline=false;
                          this.errormessagepayonline="";
                          }, 5000);
                              }
                            }
                          }else if(status.data.status == 1){
                            this.sub.unsubscribe();
                            localStorage.setItem("user_id", JSON.stringify(data.data.user_id))
                            if(localStorage.getItem("userData")){
                              var userID:any = JSON.parse(localStorage.getItem("userData") || "null").id;
                              var sessionId :any=null
                             }else{
                               var userID:any = null;
                              var sessionId :any=JSON.parse(localStorage.getItem("session_id") || "null")
                             }
                            this.apiService.emptyCart(userID, sessionId).subscribe(emptyData=>{
                              this.loader= false;
                              if(emptyData.status_code==200){
                                localStorage.removeItem("userformdetail");
                                this.router.navigate(['/order/order-history', data.data.order_id, data.data.user_id])
                              }
                            })
                          }else if(status.data.status == 2){
                            this.sub.unsubscribe();
                            this.paysaveloader = false;
                            this.errorflagpayonline=true;
                            this.errormessagepayonline="Payment declined, Please try again";
                            this.incompleteshow();
                          this.paymentfailed = "Payment declined, Please try again";
                            setTimeout(() => {
                              this.errorflagpayonline=false;
                            this.errormessagepayonline="";
                            }, 5000);
                          }
                         }else if (status.status_code == 203){
                          this.sub.unsubscribe();
                          this.paysaveloader = false;
                          this.errorflagpayonline=true;
                          this.errormessagepayonline=status.message;
                          this.incompleteshow();
                          this.paymentfailed = status.message;
                          setTimeout(() => {
                            this.errorflagpayonline=false;
                          this.errormessagepayonline="";
                          }, 5000);
                        }
                          
                        },(error)=>{
                          this.sub.unsubscribe();
                            this.paysaveloader = false;
                            if(data.txn_data.error != undefined){
                              this.errormessagepayonline=data.txn_data.error.message;
                              this.incompleteshow();
                              this.paymentfailed = data.txn_data.error.message;
                            }else{
                              this.errormessagepayonline="Payment declined, Please try again";
                              this.incompleteshow();
                              this.paymentfailed = "Payment declined, Please try again";
                            }
                            setTimeout(() => {
                              this.errorflagpayonline=false;
                            this.errormessagepayonline="";
                            }, 5000);
                        })
                      });
                    }
                  }
                }else{
                  
                  this.paysaveloader = false;
            this.errorflagpayonline=true;
            if(data.txn_data.error != undefined){
              this.errormessagepayonline=data.txn_data.error.message;
              this.incompleteshow();
              this.paymentfailed = data.txn_data.error.message;
            }else{
              this.errormessagepayonline="Payment declined, Please try again";
              this.incompleteshow();
              this.paymentfailed = "Payment declined, Please try again";
            }
            
            
            
            setTimeout(() => {
              this.errorflagpayonline=false;
            this.errormessagepayonline="";
            }, 5000);
            this.sub.unsubscribe();
                }
              }else if(data.txn_data.transactionStatus == "APPROVED"){
                // this.sub.unsubscribe();
                this.paymentstatus(orderid, data);
              }else{
 
                this.paysaveloader = false;
                this.errorflagpayonline=true;
               
                if(data.txn_data.error != undefined){
                  this.errormessagepayonline=data.txn_data.error.message;
                  this.incompleteshow();
                  this.paymentfailed = data.txn_data.error.message;
                }else{
                  this.errormessagepayonline="Payment declined, Please try again";
                  this.incompleteshow();
                  this.paymentfailed = "Payment declined, Please try again";
                }
               
                setTimeout(() => {
                  this.errorflagpayonline=false;
                this.errormessagepayonline="";
                }, 5000);
                this.sub.unsubscribe();
              }
              
              // console.log(data.txn_data.authenticationResponse.secure3dMethod.methodForm)
              
               setTimeout(() => {
               var myForm = <HTMLFormElement>document.getElementById('tdsMmethodForm');
               myForm.submit();
               }, 100);
              
         
            
             this.sub = interval(5000)
          .subscribe(() => { 
            this.apiService.paymentstatus(orderid).subscribe(async status=>{
              // console.log(JSON.parse(status.data.callback_data),"patel");
              // console.log(status,"patel");
             if(status.status_code == 200){
              if(status.data.status == 0){
                if(status.data.callback_data != null){
                  var callbackData = await JSON.parse(status.data.callback_data)
                  // console.log(callbackData);
                  if(('responseType' in callbackData)==false){
                    if(callbackData.transactionStatus == "APPROVED"){
                      this.sub.unsubscribe();
                      this.paymentstatus(orderid, data);
                    }else if(callbackData.authenticationResponse.params == "undefined"){
                      this.sub.unsubscribe();
                    this.paysaveloader = false;
                    this.errorflagpayonline=true;
                    this.errormessagepayonline="Payment declined, Please try again";
                    this.incompleteshow();
                    this.paymentfailed = "Payment declined, Please try again";
                    }
                    else{
                      this.paymentformaction  =await callbackData.authenticationResponse.params.acsURL;
                      this.paymentcreq = await callbackData.authenticationResponse.params.cReq;
                      setTimeout(() => {
                        var authenticationsubmit = <HTMLFormElement>document.getElementById('authenticationResponse');
                      authenticationsubmit.submit();
                      this.sub.unsubscribe();
                      this.paymentstatus(orderid, data);
                      }, 100);
                    }
                  
                  // this.paysaveloader = false;
                  }else{
                    this.sub.unsubscribe();
                    this.paysaveloader = false;
              this.errorflagpayonline=true;
              this.errormessagepayonline="Payment declined, Please try again";
              this.incompleteshow();
              this.paymentfailed = "Payment declined, Please try again";
              setTimeout(() => {
                this.errorflagpayonline=false;
              this.errormessagepayonline="";
              }, 5000);
                  }
                }else{
                  
                    this.callbackcount = ++this.callbackcount;
                    if(this.callbackcount == 5){
                      var orderidcvv ={
                        "token":playorderDetails.cvv,
                        "order_id":orderid.order_id
                      }

                      this.apiService.patchRequest(orderidcvv).subscribe((patchdata:any)=>{
                        // console.log(patchdata,"orderidcvv", orderidcvv)
                        if(('responseType' in patchdata.data)==true && patchdata.data.responseType =="GatewayDeclined"){
                          this.sub.unsubscribe();
                          this.paysaveloader = false;
                          this.errorflagpayonline=true;
                          this.errormessagepayonline=patchdata.data.error.message;
                          this.incompleteshow();
                          this.paymentfailed = patchdata.data.error.message;
                          setTimeout(() => {
                            this.errorflagpayonline=false;
                          this.errormessagepayonline="";
                          }, 5000);
                        }
                      })

                    }
                    
                }
              }else if(status.data.status == 1){
                this.sub.unsubscribe();
                localStorage.setItem("user_id", JSON.stringify(data.data.user_id))
                if(localStorage.getItem("userData")){
                  var userID:any = JSON.parse(localStorage.getItem("userData") || "null").id;
                  var sessionId :any=null
                 }else{
                   var userID:any = null;
                  var sessionId :any=JSON.parse(localStorage.getItem("session_id") || "null")
                 }
                this.apiService.emptyCart(userID, sessionId).subscribe(emptyData=>{
                  this.loader= false;
                  if(emptyData.status_code==200){
                    localStorage.removeItem("userformdetail");
                    this.router.navigate(['/order/order-history', data.data.order_id, data.data.user_id])
                  }
                })
              }else if(status.data.status == 2){
                this.sub.unsubscribe();
                this.paysaveloader = false;
                this.errorflagpayonline=true;
                this.errormessagepayonline="Payment declined, Please try again";
                this.incompleteshow();
                this.paymentfailed = "Payment declined, Please try again";
                setTimeout(() => {
                  this.errorflagpayonline=false;
                this.errormessagepayonline="";
                }, 5000);
              }
             }else if (status.status_code == 203){
              this.sub.unsubscribe();
              this.paysaveloader = false;
              this.errorflagpayonline=true;
              this.errormessagepayonline=status.message;
              this.incompleteshow();
              this.paymentfailed = status.message;
              setTimeout(() => {
                this.errorflagpayonline=false;
              this.errormessagepayonline="";
              }, 5000);
            }
              
            },(error)=>{
              this.sub.unsubscribe();
                this.paysaveloader = false;
                this.errorflagpayonline=true;
                if(data.txn_data.error != undefined){
                  this.errormessagepayonline=data.txn_data.error.message;
                  this.incompleteshow();
                  this.paymentfailed = data.txn_data.error.message;
                }else{
                  this.errormessagepayonline="Payment declined, Please try again";
                  this.incompleteshow();
                  this.paymentfailed = "Payment declined, Please try again";
                }
                setTimeout(() => {
                  this.errorflagpayonline=false;
                this.errormessagepayonline="";
                }, 5000);
            })
          });
            }else{
              this.paysaveloader = false;
              this.errorflagpayonline=true;
              if(data.txn_data.error != undefined){
                this.errormessagepayonline=data.txn_data.error.message;
                this.incompleteshow();
                this.paymentfailed = data.txn_data.error.message;
              }else{
                this.errormessagepayonline="Payment declined, Please try again";
                this.incompleteshow();
                this.paymentfailed = "Payment declined, Please try again";
              }
              setTimeout(() => {
                this.errorflagpayonline=false;
              this.errormessagepayonline="";
              }, 5000);
            }
            
          }else{
            this.paysaveloader = false;
          this.errorflagpayonline=true;
          this.errormessagepayonline=data.message;
          this.incompleteshow();
          this.paymentfailed = data.message;
          setTimeout(() => {
            this.errorflagpayonline=false;
          this.errormessagepayonline="";
          }, 5000);
          }
        },(error)=>{
          this.paysaveloader = false;
          this.errorflagpayonline=true;
          this.errormessagepayonline="Payment declined, Please try again";
          this.incompleteshow();
          this.paymentfailed = "Payment declined, Please try again";
          setTimeout(() => {
            this.errorflagpayonline=false;
          this.errormessagepayonline="";
          }, 5000);
        });
      }

//check payment status
paymentstatus(orderid:any, data:any){
  // console.log(orderid, data);
  this.paystatus = interval(5000).subscribe(() => { 
    this.apiService.paymentstatus(orderid).subscribe(async status=>{
      // console.log(status, data)
      if(status.data.status == 1){
        localStorage.setItem("user_id", JSON.stringify(data.data.user_id))
        if(localStorage.getItem("userData")){
          var userID:any = JSON.parse(localStorage.getItem("userData") || "null").id;
          var sessionId :any=null
         }else{
           var userID:any = null;
          var sessionId :any=JSON.parse(localStorage.getItem("session_id") || "null")
         }
        this.apiService.emptyCart(userID, sessionId).subscribe(emptyData=>{
          this.loader= false;
          if(emptyData.status_code==200){
            localStorage.removeItem("userformdetail");

            this.router.navigate(['/order/order-history', data.data.order_id, data.data.user_id])
            this.paystatus.unsubscribe();
          }
        })
      }else if(status.data.status == 2){

        this.paysaveloader = false;
        this.errorflagpayonline=true;
        this.errormessagepayonline="Payment declined, Please try again";
        this.incompleteshow();
          this.paymentfailed = "Payment declined, Please try again";
        this.paystatus.unsubscribe();
        setTimeout(() => {
          this.errorflagpayonline=false;
        this.errormessagepayonline="";
        }, 5000);
      }
    })
    })
}

// pay at resturants

placeOrder(){
  this.loader= true;
  if(localStorage.getItem("userData")){

    var playorderDetails:any={
      "store_id":this.storefilter[0].id,
      "session_id":null,
      "user_id":JSON.parse(localStorage.getItem("userData") || "null").id,
      "payment_method":this.paymentMethod,
    "payment_type":this.paymentType,
    "total_order_amount":this.totalPizzaCost,
    "tax_amount":this.iva,
    "amount_to_be_paid":this.lastTotalpriceCost,
    "tabType":JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").type == "Pickup"? "take_out": "delivery",
    // device:roid
     "special_instructions":"",
     "delivery_instructions":"",
     "first_name":JSON.parse(localStorage.getItem("userData") || "null").first_name,
     "last_name":JSON.parse(localStorage.getItem("userData") || "null").last_name,
     "email":JSON.parse(localStorage.getItem("userData") || "null").email,
    // customer_name:anjali
    "customer_mobile":this.checkoutLogedIn.get('phone')?.value,
    "customer_extention":this.checkoutLogedIn.get('extension')?.value,
    "contactless_pickup":this.checkoutLogedIn.get('contactless')?.value,
    }
  }else{
    var playorderDetails:any={
      "store_id":this.storefilter[0].id,
      "user_id":null,
      "session_id":JSON.parse(localStorage.getItem("session_id") || "null"),
    "payment_method":this.paymentMethod,
    "payment_type":this.paymentType,
    "total_order_amount":this.totalPizzaCost,
    "tax_amount":this.iva,
    "amount_to_be_paid":this.lastTotalpriceCost,
    "tabType":JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").type == "Pickup"? "take_out": "delivery",
    // device:roid
     "special_instructions":"",
     "delivery_instructions":"",
     "first_name":this.checkoutguestDetail.get('fname')?.value,
     "last_name":this.checkoutguestDetail.get('lname')?.value,
     "email":this.checkoutguestDetail.get('email')?.value,
    "customer_mobile":this.checkoutguestDetail.get('phone')?.value,
    "customer_extention":this.checkoutguestDetail.get('extension')?.value,
    "contactless_pickup":this.checkoutguestDetail.get('contactless')?.value,
    }
  }


  this.apiService.placeorder(playorderDetails).subscribe(data=>{

    if(data.status_code == 200){
       localStorage.setItem("user_id", JSON.stringify(data.data.user_id))
      if(localStorage.getItem("userData")){
        var userID:any = JSON.parse(localStorage.getItem("userData") || "null").id;
        var sessionId :any=null
       }else{
         var userID:any = null;
        var sessionId :any=JSON.parse(localStorage.getItem("session_id") || "null")
       }
      this.apiService.emptyCart(userID, sessionId).subscribe(emptyData=>{
        this.loader= false;
        if(emptyData.status_code==200){
          localStorage.removeItem("userformdetail");
          this.router.navigate(['/order/order-history', data.data.order_id, data.data.user_id])
        }
      })
    }else{
      this.loader= false;
      document.body.classList.add('doughpopup')
    }
  })

}


  addressList(){
    if(localStorage.getItem("userData")){
      this.apiService.listUSerPickUp(JSON.parse(localStorage.getItem("userData")||"null").id).subscribe(list=>{

      
        if(list.status_code == 200){
      
          if(localStorage.getItem("savedPickupLocation")){
            var arraysavedPickupLocation = JSON.parse(localStorage.getItem("savedPickupLocation") || "null");
            list.data.forEach((element:any, index:any) => {
              if(arraysavedPickupLocation.some((value:any)=> value.store_name == element.store_name)== false){
                arraysavedPickupLocation.push(element)
              }
            });
            if(arraysavedPickupLocation.filter((value:any)=> value.by_default == 1).length > 1){

              arraysavedPickupLocation.map((obj:any)=> {
                obj.by_default = 0;
              });

              arraysavedPickupLocation.filter((value:any)=> {return value.id == JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").data.id}).map((obj:any)=>obj.by_default = 1)

              this.dataservice.listAddress();
            }else if(arraysavedPickupLocation.filter((value:any)=> value.by_default == 1).length == 1){
              localStorage.setItem('savedPickupLocation', JSON.stringify(arraysavedPickupLocation));
              this.dataservice.listAddress();
            }else{
              arraysavedPickupLocation.filter((value:any)=> { return value.id == JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").data.id}).map((obj:any)=>obj.by_default = 1)
              this.dataservice.listAddress();
            }
            localStorage.setItem('savedPickupLocation', JSON.stringify(arraysavedPickupLocation));
            // arraysavedPickupLocation.push(list.data[0])
            this.showPickupStore = JSON.parse(localStorage.getItem("savedPickupLocation") || "null")
             this.pickupPopupLocation = true;
          }else{
            var arraysavedPickupLocationnew:any=[];
            list.data.forEach((element:any, index:any) => {
              arraysavedPickupLocationnew.push(element);
            });
            localStorage.setItem('savedPickupLocation', JSON.stringify(arraysavedPickupLocationnew));
            this.showPickupStore = arraysavedPickupLocationnew;
            this.dataservice.listAddress();
          }

        }else if(list.status_code == 401){
          localStorage.removeItem("user");
          localStorage.removeItem("userData");
          localStorage.removeItem("orderProduct");
          this.dataservice.productItem();
          // localStorage.removeItem("savedPickupLocation");
          this.dataservice.changeMessage("logedOut");
          this.router.navigate(['/']);
          this.dataservice.logincheckfunction(false);
           this.pickupPopupLocation = false;
        }else{
          this.pickupPopupLocation = false;
        }

      }, error=>{
        // console.log(error)
        // console.log(error.error.message)
        if(error.status==401 || error.error.message == "Token has expired"){

        }
      })
    }else{
      if(!localStorage.getItem("savedPickupLocation")){
        var ab_pickupwithoutLogin:any = []
        ab_pickupwithoutLogin.push(
          JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").data
        )
        ab_pickupwithoutLogin.map((obj:any)=>{
          obj.by_default = 1;
        })
        localStorage.setItem('savedPickupLocation', JSON.stringify(ab_pickupwithoutLogin));
        this.showPickupStore = JSON.parse(localStorage.getItem("savedPickupLocation") || "null")
        this.pickupPopupLocation = true;
      }else{
        this.showPickupStore = JSON.parse(localStorage.getItem("savedPickupLocation") || "null")
        this.pickupPopupLocation = true;
      }

      // without login address.
    }
  }
  selectClick(storeList:any){
    var savedStoreList = JSON.parse(localStorage.getItem("savedPickupLocation") || "null");
    var newsavedStoreList = savedStoreList.map((obj:any) =>
      obj.by_default === 1 ? { ...obj, by_default: 0 } : obj
  );
  var updatedStoreList = newsavedStoreList.map((obj:any) =>
  obj.id === storeList.id ? { ...obj, by_default: 1 } : obj
);

localStorage.setItem('savedPickupLocation', JSON.stringify(updatedStoreList));
this.showPickupStore = updatedStoreList
setTimeout(() => {
  this.storeAvailCheck();
}, 2000)
// console.log(storeList);
  //  check outofstock on change stores
  this.apiService.outofstock(storeList.customer_key).then(outofstockdata=>{
        // console.log(outofstockdata, "ofs", this.cartProduct);
         outofstockdata.forEach((element:any) => {
          this.cartProduct.forEach((cart:any) => {
            if(element.item_id == cart.product_id){
              // console.log("yes")
            }else{
              // console.log("no")
            }
          });
         });
      })

  }

  addressUpdate(){
    var savedStoreList = JSON.parse(localStorage.getItem("savedPickupLocation") || "null");
     this.storefilter = savedStoreList.filter((data:any)=>{
        return data.by_default == 1
    })
    this.paymentCheck = false;
  }
  /* alsoConfig-slider */
  alsoConfig = {
    arrows:true,
    infinite: true,
    dots:false,
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  FindsPopUpShow(): void {
    document.body.classList.add('Find-open')
  }
  FindsPopUpHide(): void {
    document.body.classList.remove('Find-open')
  }
  /* alsoConfig-end */

  removeItempopup(item:any){

    document.body.classList.add('removeCartpopup')
    this.removeCartItem = item;


  }
  removeItem(){

  if(this.cartProduct.length>0){
    this.apiService.removeCart(this.removeCartItem.id).subscribe(data=>{
      
      if(data.status_code == 200){
        this.dataservice.productItem();
        document.body.classList.remove('removeCartpopup')
      }

    })
  }else{
    this.emptyCartData= true;
    this.wholePage=false;
    this.wholeloader = false;
  }


  }
  doughpopupClose(){
    document.body.classList.remove('removeCartpopup')
  }
  idc:any = 'checkoutone';
  accordion(ids:any){
    if(ids == 'checkoutone'){
      this.reviewCheck = false;
      var pizzasmheading:any = document.getElementsByClassName('pizzasmheading') as HTMLCollectionOf<HTMLElement>
      pizzasmheading[0].style.color="#EF5A0D"
    }
    if(ids == 'checkouttwo'){
      this.reviewCheckAddress = false;
      this.reviewCheck = true;
      var pizzasmheading1:any = document.getElementsByClassName('pizzasmheading') as HTMLCollectionOf<HTMLElement>
    pizzasmheading1[0].style.color="#4e7026"
      var pizzasmheading:any = document.getElementsByClassName('pizzaAddressHead') as HTMLCollectionOf<HTMLElement>
      pizzasmheading[0].style.color="#EF5A0D"
    }

    if (this.idc == ids){
      this.idc = ' ';

    }
    else{
      this.idc= ids;
    }
  }
  reviewOrder(next:any){
    localStorage.setItem("orderProduct",JSON.stringify(this.cartProduct));
    this.dataservice.productItem();
    if(this.reviewCheckAddress == true){
      this.idc = "checkoutthree";
    }else{
      this.idc = next;
    }

    this.reviewCheck = true
    var pizzasmheading:any = document.getElementsByClassName('pizzasmheading') as HTMLCollectionOf<HTMLElement>
    pizzasmheading[0].style.color="#4e7026"
  }
  idn:any = ' ';
  giftaccordion(idg:any){
    if (this.idn == idg){
      this.idn = ' ';
    }
    else{
      this.idn= idg;
    }
  }
  idp:any = ' ';
  paymentaccordion(idh:any){
    this.tokencvvOpen=-1;
    this.paymentCheck = false;
    if (this.idp == idh){
      this.idp = ' ';
    }
    else{
      this.idp= idh;
      setTimeout(() => {
        var container:any = <HTMLInputElement>document.getElementsByClassName("expiryvalidated")[0];
        // console.log(container)
       
        container.onkeyup = function(e:any) {
          var target = e.srcElement || e.target;
          var maxLength = parseInt(target.attributes["maxlength"].value, 10);
          var myLength = target.value.length;
          if (myLength >= maxLength) {
              var next = target;
              while (next = next.nextElementSibling) {
                  if (next == null)
                      break;
                  if (next.tagName.toLowerCase() === "input") {
                      next.focus();
                      break;
                  }
              }
          }
          // Move to previous field if empty (user pressed backspace)
          else if (myLength === 0) {
              var previous = target;
              while (previous = previous.previousElementSibling) {
                  if (previous == null)
                      break;
                  if (previous.tagName.toLowerCase() === "input") {
                      previous.focus();
                      break;
                  }
              }
          }
        }
       }, 100);
    }
  
  }

  payAtRestaurant(paymentType:any){
    this.paymentType = paymentType;
    this.paymentMethod="Pay at Restaurant";

    this.paymentCheck=true;
  }

  quantity:number=1;
  i=1;
  j=1;
  plus(cartItems:any, index:any, rate:any){
    console.log(rate)
    // var incredecrement =document.getElementById(`incredecrement${index}`);
    document.getElementById(`incredecrement${index}`)?.style.setProperty('display', 'none');
    document.getElementById(`incrdecreLoader${index}`)?.style.setProperty('display', 'block');
     var quantityCheckout:any = document.getElementById(`checkoutQuantity${index}`);
     var parseQuantity:any = parseInt(quantityCheckout.innerHTML)
     quantityCheckout.innerHTML = ++parseQuantity;


    //  var rateCheckout:any = document.getElementById(`checkoutRate${index}`);
    // var ratenew = `${quantityCheckout.innerHTML * (cartItems.full_details.rate/cartItems.full_details.quantity)}`
    var updateCartdata = {
      "rate":rate,
      "quantity": parseInt(quantityCheckout.innerHTML),
      "cart_id":cartItems.id,
      "store_id":JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").data.id
        }
  this.apiService.updateCart(updateCartdata).subscribe(data=>{
    if(data.status_code == 200){
      this.dataservice.productItem();
      
      
          document.getElementById(`incredecrement${index}`)?.style.setProperty('display', 'flex');
        document.getElementById(`incrdecreLoader${index}`)?.style.setProperty('display', 'none');
       
 
     
    }
  })


  }
  minus(cartItems:any, index:any, rate:any){
  
    var quantityCheckout:any = document.getElementById(`checkoutQuantity${index}`);
    var parseQuantity:any = parseInt(quantityCheckout.innerHTML)
    if(parseQuantity> 1){
      document.getElementById(`incredecrement${index}`)?.style.setProperty('display', 'none');
      document.getElementById(`incrdecreLoader${index}`)?.style.setProperty('display', 'block');
    quantityCheckout.innerHTML = --parseQuantity;

    // var rateCheckout:any = document.getElementById(`checkoutRate${index}`);
    // var ratenew = `${quantityCheckout.innerHTML * (cartItems.full_details.rate/cartItems.full_details.quantity)}`
      var updateCartdata = {
        "rate":rate,
        "quantity": parseInt(quantityCheckout.innerHTML),
        "cart_id":cartItems.id,
        "store_id":JSON.parse(localStorage.getItem("pickupDeliverystores") || "null").data.id
          }
    this.apiService.updateCart(updateCartdata).subscribe(data=>{
      if(data.status_code == 200){
        this.dataservice.productItem();
       
         
            document.getElementById(`incredecrement${index}`)?.style.setProperty('display', 'flex');
          document.getElementById(`incrdecreLoader${index}`)?.style.setProperty('display', 'none');
         

      }
    })
    }


  }

  rateaddition(cartItems:any){
    var entityrate=0;
 

  }

  pickupShow(e:any) {
    this.pickupShown = true;
    this.deliveryShown = false;
    this.universityShown = false;
    this.forfutureShown = false;
    this.deliverytopShown = false;
    this.addressShown = false;
    this.tipdriverShown = false;
  }
  deliveryShow(e:any) {
      this.deliveryShown = true;
      this.deliverytopShown = true;
      this.addressShown = true;
      this.tipdriverShown = true;
      this.pickupShown = false;
      this.universityShown = false;
  }

  forfutureShow(e:any) {
    this.forfutureShown = true;
    this.deliverytopShown = true;
    this.pickupShown = false;
    this.universityShown = false;
  }
  universityShow(e:any) {
    this.universityShown = true;
    this.forfutureShown = true;
    this.deliverytopShown = true;
    this.pickupShown = false;
    this.deliveryShown = false;
  }
  fornowShow(e:any) {
    this.forfutureShown = false;
  }
  applyShow(e:any) {
    this.applyShown = true;
  }
  applynone(e:any) {
    this.applyShown = false;
  }

  StorePopUpShow(): void {
    document.body.classList.add('Store-open')
  }
  StorePopUpHide(): void {
    document.body.classList.remove('Store-open')
  }
  FindPopUpShow(): void {
    document.body.classList.add('Find-open')
  }
  FindPopUpHide(): void {
    document.body.classList.remove('Find-open')
  }

  incompleteshow():void{
    document.body.classList.add('incompleteshow')
  }
  incompleteClose():void{
    this.callbackcount = 1;
    document.body.classList.remove('incompleteshow')
    


  }

}
