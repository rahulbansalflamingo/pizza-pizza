import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SaucesDipsComponent} from './sauces-dips.component';
const routes: Routes = [
  { path: 'sauces-dips', component:  SaucesDipsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SaucesDipsRoutingModule { }
