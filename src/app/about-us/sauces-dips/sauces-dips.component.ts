import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-sauces-dips',
  templateUrl: './sauces-dips.component.html',
  styleUrls: ['./sauces-dips.component.scss']
})
export class SaucesDipsComponent implements OnInit {

  constructor(private dataservice: DataService) { }

  ngOnInit(): void {
    this.dataservice.headerFooterHide(false);
  }

}
