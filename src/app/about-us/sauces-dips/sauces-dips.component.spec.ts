import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaucesDipsComponent } from './sauces-dips.component';

describe('SaucesDipsComponent', () => {
  let component: SaucesDipsComponent;
  let fixture: ComponentFixture<SaucesDipsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaucesDipsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaucesDipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
