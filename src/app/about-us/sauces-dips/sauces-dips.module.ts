import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SaucesDipsRoutingModule } from './sauces-dips-routing.module';
import { SaucesDipsComponent } from './sauces-dips.component';


@NgModule({
  declarations: [
    SaucesDipsComponent
  ],
  imports: [
    CommonModule,
    SaucesDipsRoutingModule
  ]
})
export class SaucesDipsModule { }
