import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SaladsSidesRoutingModule } from './salads-sides-routing.module';
import { SaladsSidesComponent } from './salads-sides.component';


@NgModule({
  declarations: [
    SaladsSidesComponent
  ],
  imports: [
    CommonModule,
    SaladsSidesRoutingModule
  ]
})
export class SaladsSidesModule { }
