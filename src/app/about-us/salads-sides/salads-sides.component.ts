import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-salads-sides',
  templateUrl: './salads-sides.component.html',
  styleUrls: ['./salads-sides.component.scss']
})
export class SaladsSidesComponent implements OnInit {

  constructor(private dataservice: DataService) { }

  ngOnInit(): void {
    this.dataservice.headerFooterHide(false);
  }

}
