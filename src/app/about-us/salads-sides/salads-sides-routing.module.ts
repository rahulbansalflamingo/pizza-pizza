import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SaladsSidesComponent} from './salads-sides.component';
const routes: Routes = [
  { path: 'salads-sides', component:  SaladsSidesComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SaladsSidesRoutingModule { }
