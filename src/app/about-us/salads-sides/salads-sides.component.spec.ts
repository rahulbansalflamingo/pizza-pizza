import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaladsSidesComponent } from './salads-sides.component';

describe('SaladsSidesComponent', () => {
  let component: SaladsSidesComponent;
  let fixture: ComponentFixture<SaladsSidesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaladsSidesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaladsSidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
