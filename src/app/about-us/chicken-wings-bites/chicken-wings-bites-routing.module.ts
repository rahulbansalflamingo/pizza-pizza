import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ChickenWingsBitesComponent} from './chicken-wings-bites.component';

const routes: Routes = [
  { path: 'chicken-wings-bites', component:  ChickenWingsBitesComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChickenWingsBitesRoutingModule { }
