import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-chicken-wings-bites',
  templateUrl: './chicken-wings-bites.component.html',
  styleUrls: ['./chicken-wings-bites.component.scss']
})
export class ChickenWingsBitesComponent implements OnInit {

  constructor(private dataservice: DataService) { }

  ngOnInit(): void {
    this.dataservice.headerFooterHide(false);
  }

}
