import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChickenWingsBitesRoutingModule } from './chicken-wings-bites-routing.module';
import { ChickenWingsBitesComponent } from './chicken-wings-bites.component';


@NgModule({
  declarations: [
    ChickenWingsBitesComponent
  ],
  imports: [
    CommonModule,
    ChickenWingsBitesRoutingModule
  ]
})
export class ChickenWingsBitesModule { }
