import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChickenWingsBitesComponent } from './chicken-wings-bites.component';

describe('ChickenWingsBitesComponent', () => {
  let component: ChickenWingsBitesComponent;
  let fixture: ComponentFixture<ChickenWingsBitesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChickenWingsBitesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChickenWingsBitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
