import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, OnInit, Inject, Renderer2 } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { DataService } from 'src/app/services/data.service';
import { SeoService } from 'src/app/services/seo.service';



declare var $:any;
@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  aboutLists:any= [];
  aboutselectedList:any;
  aboutUsData:any;
  aboutusbanner:any;
  aboutourfood:any;
  ourqualitybanner:any;
  menufaq:any=[];
  raisedfaq:any=[];
  loaderHold:boolean = false;
  constructor(@Inject(DOCUMENT) private document: Document, private renderer: Renderer2, private dataservice: DataService, private apiService:ApiServiceService, private elRef:ElementRef,
  private seoData:SeoService) {
    this.dataservice.headerFooterHide(false);
   }

   forceNavigate(name: string) {
    window.location.href = `/about-us#${name}`
    
      //  this.router.navigate(['/about-us'], { fragment: name });
  }



  ngOnInit(): void {
    this.loaderHold = true;
    this.renderer.addClass(this.document.body, 'inner-footer');
    
    this.renderer.addClass(this.document.body, 'inner-footer');

    let script = this.renderer.createElement('script');
    script.type = "text/javascript";
    script.src = "assets/js/custom.js";
    
    this.apiService.aboutUs().subscribe( data=>{
      this.seoData.updatemetatags(data.data[0]);
  
       this.aboutUsData = data.data[0].cms_data.SECTION1[0].only_html;
       this.renderer.appendChild(this.document.body, script);
      this.loaderHold = false;
    })
    
  }
  ngOnDestroy(): void {
    this.renderer.removeClass(this.document.body, 'inner-footer');
  }
 
  // ngAfterViewChecked (){
  //   var antibioticsfaq1 = this.elRef.nativeElement.querySelector('#antibiotics-faq-1')
  //   antibioticsfaq1.onclick = (e:any) => {
  //     antibioticsfaq1.parentElement.classList.toggle('active');
  //   }
  // } 



  scroll(el: HTMLElement) {
    el.scrollIntoView();
  }



  // toggleShow() {
  //   this.isShown = ! this.isShown;
  // }

  idm:any = 'menu-dropdowen-1';
  accordionheader(ids:any){
    if (this.idm == ids){
      this.idm = ' ';
    }
  else{
    this.idm= ids;
  }
}

  menuid:any = ' ';
  accordion(ids:any){
  
    if (this.menuid == "menu-faq-"+ids){
      this.menuid = ' ';
    }
  else{
    this.menuid= "menu-faq-"+ids;
  }
}
raisedid:any = ' ';
raisedidaccordion(ids:any){
   
    if (this.raisedid == "menu-faq-"+ids){
      this.raisedid = ' ';
    }
  else{
    this.raisedid= "menu-faq-"+ids;
  }
}
  openMenuList(aboutLists:any){
    this.aboutselectedList = aboutLists;
    
  }

   scripts(){

    $(document).ready(function(){

    });
  }

}
