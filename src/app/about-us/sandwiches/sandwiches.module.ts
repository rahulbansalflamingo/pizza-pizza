import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SandwichesRoutingModule } from './sandwiches-routing.module';
import { SandwichesComponent } from './sandwiches.component';


@NgModule({
  declarations: [
    SandwichesComponent
  ],
  imports: [
    CommonModule,
    SandwichesRoutingModule
  ]
})
export class SandwichesModule { }
