import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SandwichesComponent} from './sandwiches.component';

const routes: Routes = [
  { path: 'sandwiches', component:  SandwichesComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SandwichesRoutingModule { }
