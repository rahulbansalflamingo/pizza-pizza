import { DOCUMENT } from '@angular/common';
import { Component, OnInit, Inject, Renderer2  } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-pizza',
  templateUrl: './pizza.component.html',
  styleUrls: ['./pizza.component.scss']
})
export class PizzaComponent implements OnInit {
  aboutLists= ['Keto Pizza', 'Cauli Pizza', 'Gourmet Thins', 'Gluten Free Pizza', 'Small Pizza', 'Medium Pizza', 'Large Pizza', 'X-Large Pizza','Walk-in Slice'];
  aboutselectedList:any;

  constructor(@Inject(DOCUMENT) private document: Document, private renderer: Renderer2, private dataservice: DataService) { }

  ngOnInit(): void {
    this.renderer.addClass(this.document.body, 'inner-footer');
    this.dataservice.headerFooterHide(false);
    this.aboutselectedList = this.aboutLists[0];
  }
  ngOnDestroy(): void {
    this.renderer.removeClass(this.document.body, 'inner-footer');
  }

id:any = ' ';
  accordion(ids:any){
    if (this.id == ids){
      this.id = ' ';
    }
  else{
    this.id= ids;
  }
}

openMenuList(aboutList:any){
  this.aboutselectedList = aboutList;
}

}
