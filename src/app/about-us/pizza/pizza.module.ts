import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PizzaRoutingModule } from './pizza-routing.module';
import { PizzaComponent } from './pizza.component';
import { SideHeaderModule } from 'src/app/side-header/side-header.module';


@NgModule({
  declarations: [
    PizzaComponent
  ],
  imports: [
    CommonModule,
    PizzaRoutingModule,
    SideHeaderModule
  ]
})
export class PizzaModule { }
