import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PanzerottisComponent} from './panzerottis.component';

const routes: Routes = [
  { path: 'panzerottis', component:  PanzerottisComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PanzerottisRoutingModule { }
