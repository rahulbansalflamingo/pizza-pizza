import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-panzerottis',
  templateUrl: './panzerottis.component.html',
  styleUrls: ['./panzerottis.component.scss']
})
export class PanzerottisComponent implements OnInit {

  constructor(private dataservice: DataService) { }

  ngOnInit(): void {
    this.dataservice.headerFooterHide(false);
  }

}
