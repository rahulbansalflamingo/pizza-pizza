import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PanzerottisRoutingModule } from './panzerottis-routing.module';
import { PanzerottisComponent } from './panzerottis.component';


@NgModule({
  declarations: [
    PanzerottisComponent
  ],
  imports: [
    CommonModule,
    PanzerottisRoutingModule
  ]
})
export class PanzerottisModule { }
