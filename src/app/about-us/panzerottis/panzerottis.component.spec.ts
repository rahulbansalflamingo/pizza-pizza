import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PanzerottisComponent } from './panzerottis.component';

describe('PanzerottisComponent', () => {
  let component: PanzerottisComponent;
  let fixture: ComponentFixture<PanzerottisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PanzerottisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PanzerottisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
