import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DessertsRoutingModule } from './desserts-routing.module';
import { DessertsComponent } from './desserts.component';


@NgModule({
  declarations: [
    DessertsComponent
  ],
  imports: [
    CommonModule,
    DessertsRoutingModule
  ]
})
export class DessertsModule { }
