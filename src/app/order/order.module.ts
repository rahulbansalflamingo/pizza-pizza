import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderRoutingModule } from './order-routing.module';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { WebLoaderModule } from '../web-loader/web-loader.module';
import { OrderListComponent } from './order-list/order-list.component';


@NgModule({
  declarations: [
    OrderHistoryComponent,
    OrderListComponent
  ],
  imports: [
    CommonModule,
    OrderRoutingModule,
    WebLoaderModule
  ]
})
export class OrderModule { }
