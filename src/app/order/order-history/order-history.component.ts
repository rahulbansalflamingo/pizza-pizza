import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { DataService } from 'src/app/services/data.service';
import { setInterval } from 'timers';
import {interval, Observable, Subscription} from 'rxjs';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.scss']
})
export class OrderHistoryComponent implements OnInit, OnDestroy {
orderId:any;
orderDetails:any;
cancelHide: boolean = true;
isloader:boolean=false;
totalPizzaCost:any;
loader:boolean= false;
gst:any = 2.68;
pst:any = 2.68;
statusOrder:any;
orderData:any
wholepage:boolean=false;
sub!:Subscription
  constructor(private activatedRoute: ActivatedRoute,private apiService:ApiServiceService,private dataservice: DataService,private router: Router) {
    this.dataservice.cartshow(false);
   }

  ngOnInit(): void {
    jQuery(".find-popup").removeClass("is-visible");
    this.dataservice.productItem();

    console.log(this.router);
    // alert("hide");

    this.activatedRoute.params.subscribe(params => {
      const accountid= params['accountid'];

      this.orderId = params['slug']
      var userID = params['userId']

      this.apiService.orderDetail(userID ,this.orderId).subscribe(data=>{
        this.orderData = data.data[0]
        this.orderDetails = data.data[0].order_details;

        this.totalPizzaCost = data.data[0].order_details.reduce((accumulator:number, object:any) => {
          return accumulator + parseInt(object.full_details.rate);
        }, 0);
        var orderdate = new Date(data.data[0].created_at);
        var currentDate = new Date();
        // if((orderdate.getTime()+30000)< currentDate.getTime()){
        //   this.cancelHide=false
        // }
        this.wholepage= true;
        this.isloader = true;
  });

//     this.orderId = this.activatedRoute.snapshot.params['slug']
// var userID = this.activatedRoute.snapshot.params['userId']


// var orderID = this.activatedRoute.snapshot.params['slug']



})





// for cancel order

// setTimeout(() =>{

// },30000)



this.orderStatus(this.orderId);
this.sub = interval(10000)
    .subscribe(() => {
      this.orderStatus(this.orderId);
    });
  }

  // cancel order function
  cancelOrderFunction(orderId:any){
    document.body.classList.add('cancelpopup')


  }
  cancelpopup(orderId:any){
    this.loader = true;
    var cancelData = {
      "order_id": orderId,
    }
    this.apiService.orderCancel(cancelData).subscribe(data=>{
      if(data.status_code==200){
        this.loader = false;
        this.orderStatus(orderId);
        document.body.classList.remove('cancelpopup')
      }else{
        this.loader = false;
        document.body.classList.remove('cancelpopup')
        document.body.classList.add('doughpopup')

      }
    })

  }
  doughpopupClose():void{
    document.body.classList.remove('cancelpopup')
  }

  neworder(){
    this.dataservice.productItem();
    this.router.navigate(['/']);

  }

  orderStatus(orderId:any){
    this.apiService.orderStatus(orderId).subscribe(data=>{

      if(data.status_code ==200){
        if(data.data.cancelled == 1){
          this.cancelHide=false
          this.statusOrder = "Cancelled";
          this.sub.unsubscribe();
          this.cancelHide=false;
        }
        if(data.data.accepted == 1){
          this.cancelHide=false
          this.statusOrder = "Accepted";
        }
        if(data.data.rejected == 1){
          this.cancelHide=false
          this.statusOrder = "Rejected";
          this.sub.unsubscribe();
          this.cancelHide=false;
        }
         if(data.data.settled == 1){
          this.cancelHide=false
          this.statusOrder = "Ready for Pick up";
          this.cancelHide=false;
          this.sub.unsubscribe();
        }
        if(data.data.delivered == 1){
          this.cancelHide=false
          this.statusOrder = "Delivered";
          this.sub.unsubscribe();
          this.cancelHide=false;
        }

        if(data.data.cancelled == 0 && data.data.delivered == 0 && data.data.settled == 0 && data.data.rejected == 0 && data.data.accepted == 0){

          this.statusOrder = "Pending";
        }
      }
    })
  }

  ngOnDestroy(){
    // this.sub.unsubscribe();

  }

}
