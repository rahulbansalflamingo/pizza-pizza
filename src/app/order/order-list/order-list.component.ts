import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiServiceService } from 'src/app/services/api-service.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {
userId:any;
isloader:boolean = false;
wholePage:boolean = false;
orderData:any;

  constructor(private activatedRoute: ActivatedRoute,private apiService:ApiServiceService,private dataservice: DataService) { }

  ngOnInit(): void {
    this.dataservice.productItem();
    this.isloader= false;
    this.userId = JSON.parse(localStorage.getItem("userData") || "null").id;
    this.apiService.orderList(this.userId).subscribe(data=>{
      this.orderData = data.data;
      this.wholePage = true;
      this.isloader=true;
    })
  }

  

}
