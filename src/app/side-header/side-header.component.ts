import { Component, OnInit } from '@angular/core';
import { ViewportScroller } from "@angular/common";
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data.service';


@Component({
  selector: 'app-side-header',
  templateUrl: './side-header.component.html',
  styleUrls: ['./side-header.component.scss']
})
export class SideHeaderComponent implements OnInit {
  isFixedNavbar:any;
  activeIndex = 0;
  liActiveTab:any;
  idm:any ="about-us";
  constructor(private scroller: ViewportScroller, private dataservice: DataService, private router:Router, private activerouter:ActivatedRoute) { }

  ngOnInit(): void {
   
    if(this.activerouter.snapshot.routeConfig?.path == "about-us"){
      this.liActiveTab = "about-us";
    }else if(this.activerouter.snapshot.routeConfig?.path == "franchising"){
      this.liActiveTab = "franchising";
    }
    else if(this.activerouter.snapshot.routeConfig?.path == "company"){
      this.liActiveTab = "company";
    }
    else if(this.activerouter.snapshot.routeConfig?.path == "investors"){
      this.liActiveTab = "investors";
    }
    else if(this.activerouter.snapshot.routeConfig?.path == "for-you"){
      this.liActiveTab = "for-you";
    }
    this.idm = this.liActiveTab;
    
  }
  movetoSection(fragment:any, url:any) {
    this.router.navigate([url])
    this.scroller.scrollToAnchor(fragment);
  }
  toggleNavbar() {
    if(this.isFixedNavbar) {
      this.isFixedNavbar = false;
    } else {
      this.isFixedNavbar = true;
    }
  }
  gobackhome(){
    this.dataservice.headerFooterHide(true);
    this.router.navigate(['/']);
  }

  // scroll(el: HTMLElement) {
  //   el.scrollIntoView();
  // }

  // toggleShow() {
  //   this.isShown = ! this.isShown;
  // }

 
  accordionheader(ids:any){
    if (this.idm == ids){
      this.idm = ' ';
    }
  else{
    this.idm= ids;
  }
}


   scripts(){

    $(document).ready(function(){

    });
  }

}
