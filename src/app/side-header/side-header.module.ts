import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideHeaderComponent } from './side-header.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [SideHeaderComponent],
  exports:[SideHeaderComponent],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class SideHeaderModule { }
