import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class InterceptorInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let tokenizedReq = request.clone({
      // setHeaders:{
      //   Authorization: "Basic cjFMNV9yZ1hjOnhVaGtvOXB6d3VCN2xCWXRRbE1WUXdaZExLTityRmZTdDdycmFMZUMvZ2s9"
      // }
    })
    return next.handle(tokenizedReq);
  }
}
