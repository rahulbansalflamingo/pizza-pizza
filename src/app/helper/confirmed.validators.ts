import { AbstractControl, FormGroup, ValidatorFn } from "@angular/forms";
export function ConfirmPasswordValidator(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      let control = formGroup.controls[controlName];
      let matchingControl = formGroup.controls[matchingControlName]
      if (
        matchingControl.errors &&
        !matchingControl.errors['confirmPasswordValidator']
      ) {
        return;
      }
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ confirmPasswordValidator: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }



  // expiry date validator

  export function expirymonthValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      // const [month, year] = control.value.split('/');


      const month = control.value;
      if((month>12 || month<1) && month.length>0){
  
          return { 'invalidMonth': true };
      }
      
      return null;
    };
  }

  export function expiryyearValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      const year = control.value;
      const inputyear = 20+year;
      
      const currentyear = new Date().getFullYear();
 
      if((inputyear >currentyear+15 || inputyear<currentyear) && year.length>0){
      
        return { 'invalidyear': true };
      }

   
      return null;
    };
  }
  