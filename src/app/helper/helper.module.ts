import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SanitizerPipe } from './sanitizer.pipe';
import { CheckScriptsPipe } from './sanitizer.pipe';
import { RunScriptsDirective } from './run-scripts.directive';



@NgModule({
  declarations: [SanitizerPipe, RunScriptsDirective, CheckScriptsPipe],
  exports:[SanitizerPipe, CheckScriptsPipe],
  imports: [
    CommonModule
  ]
})
export class HelperModule { }
