import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoughPopupsComponent } from './dough-popups.component';



@NgModule({
  declarations: [
    DoughPopupsComponent
  ],
  exports:[DoughPopupsComponent],
  imports: [
    CommonModule
  ]
})
export class DoughPopupsModule { }
