import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoughPopupsComponent } from './dough-popups.component';

describe('DoughPopupsComponent', () => {
  let component: DoughPopupsComponent;
  let fixture: ComponentFixture<DoughPopupsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoughPopupsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoughPopupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
